<<?PHP

/**
 * $Id: index.php 86 2010-07-08 13:26:12Z bajt $
 * --------------------------------------------
 * Default index file.
 *
 * @package RRsoft-CMS
 * @version $Rev: 30 $ $Date$ $Author: bajt $
 * @copyright (c) 2009-2010 RRsoft www.rrsoft.cz
 * @license GNU Public License
 *
 * Minimum Requirement: PHP 5.3.8
 */
//namespace App;

use Wbengine\Config;
use App\WebApp;

define('IN_CMS', true);

error_reporting(E_ALL);

chdir(dirname(__DIR__));

define('APP_DIR', dirname(__DIR__) . '/App');

header('Content-type: text/html; charset=utf-8');

include dirname(__DIR__) . '/vendor/autoload.php';


try {

    $configPath = dirname(__DIR__) . '/App/Config/Devel.cfg.php';

    $App = New WebApp();

    $App->init(Config::autodetectEnvironment())->run(HTML_ERROR_410);

} catch (\Wbengine\Exception\RuntimeException $e) {

    echo('<h1>Caught Exception: Wbengine\Exception\RuntimeException</h1><p>'

        . $e->getMessage() . "</p><p> in file: " . $e->getFile() . " ({$e->getLine()})</p>");
}

