<?PHP

/**
 * $Id: index.php 86 2010-07-08 13:26:12Z bajt $
 * --------------------------------------------
 * Default index file.
 *
 * @package RRsoft-CMS
 * @version $Rev: 30 $ $Date$ $Author: bajt $
 * @copyright (c) 2009-2010 RRsoft www.rrsoft.cz
 * @license GNU Public License
 *
 * Minimum Requirement: PHP 5.3.8
 */

use App\WebApp;
use Wbengine\Config;
use Wbengine\Application\Application;

define('IN_CMS', true);

error_reporting(E_ALL);

chdir(dirname(__DIR__));

define('APP_DIR', dirname(__DIR__) . '/App');


header('Content-type: text/html; charset=utf-8');

include dirname(__DIR__) . '/vendor/autoload.php';

try {

    $debug= true;

    // @todo This setings maybe done by Application...
//    date_default_timezone_set(Config::getTimeZone());

    // take env type from apache vhost...
    //$HOST_ENV_VAL = (boolean)apache_getenv("devel");
    $HOST_ENV_VAL = false;

    /**
     * ...or we can add safe spec. types for auto detection by hostname or IP address...
     * Config::addEnvironmentSafeKeyword("devel");
     * Config::addEnvironmentSafeIp("192.168.0.0/16");
     */
    $App = New WebApp(Application::APPLICATION_TYPE_FRONT);

    /**
     * Alternates of call APP configuration depend to diff environments
     *
     * $App->init('Devel.cfg.php')->run();
     * $App->init(Config::autodetectEnvironment(Config::DETECT_ENV_TYPE_BY_HOSTNAME))->run();
     * $App->init(Config::autodetectEnvironment(Config::DETECT_ENV_TYPE_BY_IP))->run();
     */

    $App->init(Config::autodetectEnvironment($HOST_ENV_VAL))->run();

} catch (\Wbengine\Exception\RuntimeException $e) {

    $string = sprintf(file_get_contents(dirname(__DIR__).'/Exception.html'), get_class($e), $e->getCode(), $e->getMessage(), $e->getFile(), $e->getLine(), $e->getTraceAsString());

    if (isset($debug) === false) {
        error_log("Cauth Exception: " . get_class($e) . ", in object:" . $e->getMessage() . $e->getCode() .", in file:" . $e->getFile() . "(" . $e->getLine() . ")", 0);
    } else {
        echo($string);
    }
}

