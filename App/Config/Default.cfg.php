<?php

return array(
    /**
     * DB config files
     */
    'dbAdapterDefinition' => array(
        'adapterName' => 'Zend',
        'driver' => 'Pdo_Mysql',
        'dbname' => 'rrsoft_cz',
        'charset' => 'utf8',
        'lazy' => TRUE,
        'port' => '3306',
        'host' => 'mysql',
        'username' => 'rrsoft_cz',
        'password' => '6v6lI73Mcs'
    ),
    /**
     * Force allowed IP of admins
     */
    'ipAdmins' => array(
        '85.70.14.116',
        '194.213.42.148',
        '62.77.86.182'
    ),
    /**
     * Site default code page
     */
    'codePage' => 'UTF-8',
    /**
     * Enable/Disable debug mode
     */
    'debug' => false,
    /**
     * Default PHP timezone
     */
    'timeZone' => 'Europe/Prague',
    /**
     * Minimize CSS when source has changed...
     */
    'minimizeCss' => true,
    'minimizeHtml' => true,
    'minimizeJs' => false,
    /**
     * All css includes
     */
    'cssFiles' => array(
        '/Public/Css/default.css',
        '/Public/Css/responsive.css',
        '/Public/Css/backend.css',
        '/Public/Css/colorbox.css',
        '/Public/Css/slider.css',
    ),
    'templateDirPath' => '/Src/View/',
    // 'cdnPath' => 'https://2e54af90.cdn-port.com',
    'cdnPath' => 'http://2e54af90.cdn-port.com',
);
