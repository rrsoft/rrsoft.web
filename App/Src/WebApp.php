<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WebApp
 *
 * @author roza
 */

namespace App;

use App\Src\Exception\AppException;
use Wbengine\Application\Application;
use Wbengine\Application\Path\Path;
use Wbengine\Config;
use Wbengine\Config\Adapter\AdapterInterface;
use Wbengine\Db\Db;
use Wbengine\Exception;
use Wbengine\Registry;
use Wbengine\Db\Exception\DbException;
use Wbengine\Site\SiteException;
use Zend\Db\TableGateway\Exception\RuntimeException;
use Wbengine\Application\Path\File;

//use Wbengine\Config;

class WebApp extends Application
{
//    CONST FRONT_ID = 0;
//    CONST BACK_ID = -1;

//    CONST APP_TYPE_FRONT            = 'Front';
//    CONST APP_TYPE_BACKEND          = 'Admin';

    CONST APP_BASE                  = '/App';
    CONST APP_CONFIG_PATH           = '/Config/';
    CONST APP_TEMPLATE_PATH         = '/Src/View';
    CONST APP_TYPE_CACHE            = '/Cache';
    CONST APP_TYPE_RENDERER_TEMP    = '/Renderer';

    public function __construct($appType = null)
    {

        if(is_null($appType)){
            Throw New AppException(
                sprintf('%s->%s : Application type cannot be empty.', __CLASS__, __FUNCTION__));
        }

        $this->appType      = $appType;

        if($appType == self::APPLICATION_TYPE_BACKEND) {
            $this->isBackend = TRUE;
        }elseif($appType == self::APPLICATION_TYPE_FRONT) {
            $this->isBackend = FALSE;
        }else {
            $this->isBackend = FALSE;
        }



//        $this->setPath(Path::TYPE_BASE, (__DIR__));
//        $this->setPath(Path::TYPE_CONFIG, self::APP_CONFIG_PATH);
//        var_dump($this->_getObjectPath());
//        var_dump($this->getPath(Path::TYPE_CONFIG));

        // Setup error handling...
        set_error_handler(array($this->getErrorHandler(), 'SetErrorHandler'));

        // Set app template dir...
//        $this->setTemplateDir(__DIR__ . self::APP_TEMPLATE_PATH);
    }


    /**
     * Initialize master app object for use...
     * @param string $configFileName
     * @throws Exception\RuntimeException
     * @throws \Exception
     * @return \Wbengine\Application\Application
     */
    public function init($configFileName)
    {

//        Config::setConfigFilePath(APP_DIR.self::APP_CONFIG_PATH.$configFileName);

//        var_dump($configFileName);
        // check config adapter...
//        if (!$configFileName instanceof ConfigAdapterInterface) {
//
//            throw new \Exception(sprintf(
//                '%s expects a instance of class Wbengine\Config argument; received '%s''
//                , __METHOD__, (is_object($configFileName) ? get_class($configFileName) : gettype($configFileName))
//            ));
//        }


//var_dump(dirname(__DIR__));
        try {

        /**
         * 1. SETUP PATHS
         */
        $this->setPath(Path::TYPE_BASE, (dirname(__DIR__)));
        $this->setPath(Path::TYPE_TEMPLATES, self::APP_TEMPLATE_PATH.$this->getAppTypeId());
        $this->setPath(Path::TYPE_CACHE, self::APP_TYPE_CACHE);
        $this->setPath(Path::TYPE_RENDERER_TEMP, self::APP_TYPE_RENDERER_TEMP, true);
        $this->setPath(Path::TYPE_APP_BRAND, $this->getAppType());
//var_dump(self::APP_TYPE_RENDERER_TEMP);

//Config::test();
//        ;
//        $this->setConfig(Config::load(New Path(Path::TYPE_BASE, __DIR__, true), Config::ADAPTER_TYPE_ARRAY));
//        $this->setTemplateDir(APP_DIR . $this->getConfig()->getTemplateDirPath(HTML_TEMPLATE_TYPE_FRONT));
        /**
         * 2. LOAD CONFIGURATION
         * Setup config file example over class setter...
         * Config::setConfigFilePath(APP_DIR.self::APP_CONFIG_PATH.$configFileName);
         */

        // And with class Path...
        $this->setConfig(Config::load(New File(APP_DIR.self::APP_CONFIG_PATH.$configFileName, true)));

//        $this->setConfig(Config::load($this));

        /**
         * 3. FIST TIME INIT OBJECT SITE
         */

//        $this->setPath(Path::TYPE_APP_TYPE, $this->getSite()->getSiteTypeKey());

//var_dump($this->getSite()->getSiteTypeKey());

        // store config adapter...
//        $this->setConfig(Config::getAdapter());
//var_dump($this->getPath(Path::TYPE_RENDERER_TEMP));

//var_dump(Config::getCssCollection()->toArray());
            if(Config::minimizeCss() === true) {
                $this->minimizeCssFiles(Config::getCssCollection()->toArray(), dirname(APP_DIR));
            }
//            $db = New Db($this->getConfig()->getDbCredentials());
//            $dbAdapter = $db->getAdapter();
//
//            // prepare PDO connection...
//            Registry::set('db', $dbAdapter);

            // set default time zone...
//            date_default_timezone_set($this->getConfig()->getTimeZone());
            date_default_timezone_set(Config::getTimeZone());

//var_dump($this->getSite());
            // Setup custom App variables for the renderer...
            // Go to class \Wbengine\Site ->init() method for the template variable essential assigments.
//            $this->setValue('user', $this->getIdentity()->username);
//            $this->setValue('locale', $this->getLocale()->getAllKeywords());
//            $this->setValue('defaultCssFile', $this->getConfig()->getCssCollection(), 'config');
//            die(fffff);
            $this->getSite()->initialize($this);
//            var_dump($this->getAppTypeId());
//            die(ddddd);


        } catch (SiteException $e) {
//die(var_dump($this->_getObjectPath()->getAllPaths()));
            //@todo We need to solve all other error codes...
            $this->addException('Site not found.', HTML_ERROR_404);
//            $this->setValue(HTML_HEADER_SECTION, $this->getRenderer()->render('Central/slider'));
            $this->setValue(HTML_CENTRAL_SECTION, $this->getRenderer()->getErrorBox($e));
//            throw new Exception\RuntimeException($e->getMessage(), $e->getCode());
        }

        return $this;
    }

}
