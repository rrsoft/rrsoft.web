<?php

/**
 * Description of AppException
 *
 * @author roza
 */

namespace App\Src\Exception;

use Wbengine\Application\ApplicationException;
use Wbengine\Exception;

class AppException extends ApplicationException {

}
