<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="Content-language" content="cs"/>
    <meta name="Keywords" content="{$meta.keywords}"/>
    <meta name="Description" content="{$meta.description}"/>
    <meta name="resource-type" content="document"/>
    <meta name="rating" content="General"/>
    <meta name="author" content="Radek Roža - RRsoft"/>
    <meta name="copyright" content="Copyright © 2014 RRsoft, All rigts reserverd. Email:  office@rrsoft.cz"/>
    <meta name="robots" content="index,follow"/>
    <meta http-equiv="Imagetoolbar" content="no"/>
    <meta http-equiv="Cache-control" content="no-cache"/>

    <link rel=”author” href="https://www.google.com/+RadekRoza" />
    <link rel="StyleSheet" type="text/css" href="{$cdn.hostname}/Css/backend-min.css" />
    {*<link rel="StyleSheet" type="text/css" href="/Css/Slider.css" media="screen"/>*}
    {*<link rel="StyleSheet" type="text/css" href="/Css/colorbox.css" media="screen"/>*}
    {*<link rel="StyleSheet" type="text/css" href="/Css/fonts.css" media="screen"/>*}
    <link rel="StyleSheet" type="text/css" href="{$cdn.hostname}/Css/responsive-min.css" media="screen and (max-width:960px)"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>|{$meta.title}</title>
    <script src="{$cdn.hostname}/Js/jquery-1.11.1.min.js"></script>
    {*<script src="/Js/jquery.colorbox-min.js"></script>*}
    {*<script src="/Js/jquery-ui.js"></script>*}
    {*<script src="/Js/jquery.bxslider.min.js"></script>*}
    {*<script src="/Js/social-buttons.js"></script>*}
</head>
<body>
<div id="top-navigator">
    <div class="inner">
        <ul class="left">
            <li class="home-ico"><a class="home-ico" href="/">Home</a></li>
        </ul>
        <ul class="right">
            <li class="phone-icoxx"><span class="phone-ico">Podpora 24 / 7: +420 733 378 437</span></li>
            <li><a class="mail-ico" href="/">support@rrsoft.cz</a></li>
            <li class="last"><a class="login-ico" href="/login/">Přihlásit se</a></li>
            <li><a class="contact-ico" href="/kontakty/">Kontakty</a></li>
        </ul>
    </div>
</div>
<div id="main_wrap">
    <!--STAR MAIN MENU-->
    <div id="menu">
        <ul>
            {foreach $menu as $mItemitem}
                <li>
                    <a href="{$mItemitem.url}"
                       class="selected_{$mItemitem.selected}" title="{$mItemitem.description}">
                        <span class="menuSpan">{$mItemitem.name}
                            {*<br/><span class="subli">{$mItemitem.description}</span>*}
                        </span>
                    </a>

                </li>
            {/foreach}
        </ul>
    </div>
    <!--END MAIN MENU-->
    <div id="main">
        <!--START UNDERHEADER SPACER-->
        <div id="underheader{$html_surfix}"></div>
        <!--END UNDERHEADER SPACER-->
        <!-- START CENTRAL BOX -->
        <div id="central_box{$html_surfix}">
            {$central}
        </div>
        <!-- END CENTRAL BOX -->
        <!-- START RIGHT BOX -->
        <div id="right_box{$html_surfix}">{$right_box}&nbsp;</div>
        <!-- END RIGHT BOX -->

        <!-- START STORY BOX FOOTER -->
        <div class="clear"></div>
        {if !$html_surfix}
            <div id="central_box_footer"><a class="icon-up" href="#top-navigator" alt="Navigovat nahoru.." title="Přejít nahoru"></a></div>
        {/if}
        <!-- END STORY BOX FOOTER -->
        <div class="clear"></div>
    </div>
</div>
</body>
</html>

