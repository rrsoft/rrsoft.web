<div id="vmware-splash">
    <h1 class="left">Servis a poradenství v oboru virtualizcí</h1>
    <div class="vmware-right">
    <h3><a href="/konsolidace-fyzickych-serveru-virtualizace/">Virtualizujte své datové centrum a získejte tak výhody, které ve světě fyzických serverů nikdy nenajdete!</a></h3>
    <p class="citace"><a href="/konsolidace-fyzickych-serveru-virtualizace/">Konsolidací fyzických serverů šetříte systémovými prostředky a zapouzdřením aplikací do virtuálního prostředí, znatelně zvyšujete zabezpečení proti výpadkům i proti ztrátě dat!</a>
        </p>
    </div>
</div>
