<div class="story_box">

    <h1 class="addons-list-title"><a href="/world-of-warcraft/">World Of Warcraft</a> &raquo; Addony</h1>

    <p>Na této stránce se nacházejí vydané patche (aktualizační balíčky) do hry World Of Warcraft.<br/>
        {*<span class="citace"><b>Upozornění!</b> Tento seznam aktualizací obsahuje pouze alíčky s historií addonů!</span></p>*}


    <h2>Seznam nalezených verzí addonu {$keyword|upper}:</h2>


    {foreach from = $search_addons_patches_list item = patch key = key}
        <div class="patch_list">
            <ul class="patch_list">
                <li class="patch_list">
                    <div class="left">
                        <span class="patch_id">{$patch.tag}</span>
                    </div>
                    {*<div class="internal">*}
                    <div class="left caption">
                        <h3>
                            <a href="/world-of-warcraft/addons/patch-{$patch.patch}/{$keyword}/" >{$patch.name} v{$patch.version}</a> &raquo; pro patch &raquo;
                            <a href="/world-of-warcraft/addons/patch-{$patch.tag|replace:'.':''}/" >Patch {$patch.tag}</a>
                        </h3>
                        {*<a href="/world-of-warcraft/addons/patch-{$patch.tag|replace:'.':''}/">Seznam addonů</a>*}
                        {*kompatibilních s verzí World of Warcraft Patch {$patch.tag}*}
                    </div>
                    <div class="patch_list_clear left">
                        <span class="span">Datum: <span class="values">{$patch.released}</span></span>
                        <span class="span">Kompilace: <span class="values">{$patch.version}</span></span>
                        <span class="span">Toc file ID: <span class="values">{$patch.toc}</span></span>
                    </div>
                    {*</div>*}
                </li>
            </ul>

        </div>
    {/foreach}
    <div class="clear"></div>

</div>
