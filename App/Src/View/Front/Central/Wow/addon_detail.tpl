<script type="text/javascript" src="/Js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/Js/jquery.colorbox-min.js"></script>
<script type="text/javascript">
    /**
     * COLORBOX - definition
     */
    $(document).ready(function(){
        $(".link").colorbox();
    });
</script>
<script type="text/javascript">
    /**
     * TWITTER - implementation
     */
    window.twttr=(function(d,s,id){
        var js,fjs=d.getElementsByTagName(s)[0],t=window.twttr||{};
        if(d.getElementById(id))return;js=d.createElement(s);
        js.id=id;js.src="https://platform.twitter.com/widgets.js";
        fjs.parentNode.insertBefore(js,fjs);
        t._e=[];t.ready=function(f){
            t._e.push(f);
        };return t;
    }(
            document,"script","twitter-wjs")
    );
</script>
<script type="text/javascript">
    /**
     * FACEBOOK - declaration
     */
    window.fbAsyncInit = function () {
        FB.init({
            appId: '996786683667586',
            xfbml: true,
            version: 'v2.2'
        });
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<div class="story_box">
    {if $error }
        <h1 class="addon-title">{$error}</h1>
    {else}
        <h1 class="addon-title">{$addon_detail->name} <span class="small">v{$addon_detail->version}</span></h1>
        <!-- TOP SOCIAL PANEL IMPLEMENTATION START -->
        <div id="social-panel">
            <div
                    class="fb-share-button"
                    data-share="true"
                    data-href="http://www.rrsoft.cz{$url}"
                    data-layout="button_count"
                    data-show-faces="true">
            </div>
            <div class="g-plus" data-action="share" data-annotation="bubble"
                 data-href="http://www.rrsoft.cz{$url}"></div>
            <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.rrsoft.cz{$url}"
               data-via="rrsoftcz" data-related="rrsoft">Tweet</a>
            {if $article.id}
                <span class="span right">Publikováno: {$article.published|date_format},&nbsp;
                <span style="float: right">
                    Autor: <a href="https://www.google.com/+RadekRoza?rel=author">{$article.author}</a>
                </span>
    </span>
            {/if}
        </div>
        <!-- TOP SOCIAL PANEL IMPLEMENTATION END -->
        <h5 class="no-letter-space"><a href="/world-of-warcraft/">World Of Warcraft</a> &raquo; <a
                    href="/world-of-warcraft/addons/">Addony</a> &raquo; <a
                    href="/world-of-warcraft/addons/patch-{$addon_detail->patch}/">Patch {$addon_detail->patch_label}</a>
        </h5>
        <p>{$description}</p>
        <div class="addon_detail_list">

            <div class="images">
                <ul>
                    {foreach from = $addon_detail->getAddonImages($addon_detail->id) item = image key = key}
                        <li class="image">
                            <a class="link" href="{$image.link}" rel="link">
                                <img class="img" src="{$image.link}" alt="{$image.alt} - obrázek {$key+1}"/>
                            </a>
                        </li>
                    {/foreach}
                </ul>
            </div>
        </div>
        <div class="clear"></div>

        <div>
            <h2 class="no-letter-space"><a class="download" href="{$site_home_url}{$addon_detail->download_link}"></a>
            </h2>
        </div>
        <!-- start facebook comment plugin -->
        <p>&nbsp;</p>
        <div class="fb-comments" data-href="http://www.rrsoft.cz{$url}" data-width="100%" data-numposts="5" data-colorscheme="light"></div>
        <!-- end facebook comment plugin -->
    {/if}

</div>