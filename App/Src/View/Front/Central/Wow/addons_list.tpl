<script type="text/javascript" src="/Js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/Js/jquery.colorbox-min.js"></script>
<script type="text/javascript">
/**
* COLORBOX - definition
*/
$(document).ready(function(){
$(".link").colorbox();
});
</script>
<div class="story_box">

    <h1 class="addons-list-title"><a href="/world-of-warcraft/" title="World Of Warcraft">WoW</a> &raquo; <a
                href="/world-of-warcraft/addons/">Addony</a> &raquo; Patch {$patch_label}</h1>

    <p class="hidden-320">&nbsp;</p>

    {foreach from = $addons_list item = addon key = key}
        <div class="addons_list_main">
            <ul class="ul">
                <li class="addons_list">
                    <div><h2><a class="title" href="{$addon->detail_url}">{$addon->name}</a> ver. {$addon->version}</h2>
                    </div>
                    <div class="images">
                        <ul class="ul">
                            {foreach from = $addon->getAddonImages($addon->id) item = image key = key}
                                <li class="image">
                                    <a class="link" href="{$image.link}" rel="{$addon->id}">
                                        <img class="img" src="{$image.link}" alt="Worl Of Warcraft addon"/>
                                    </a>
                                </li>
                            {/foreach}
                        </ul>
                    </div>
                    <div class="addons_list_clear"><span class="span">Nahráno: <span
                                    class="values">{$addon->datum}</span></span>
                        <span class="span">Hodnocení: <span class="values">{$addon->rank}</span></span>
                        <span class="span">Staženo: <span class="values">{$addon->download}</span></span>
                        <span class="span">Komentářů: <span class="values">{$addon->comments}</span></span>
                    </div>
                </li>
            </ul>

        </div>
    {/foreach}
</div>