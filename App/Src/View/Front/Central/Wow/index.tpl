<div class="story_box">

    <h1>Addony do hry World of Warcraft</h1>

    <p>Vítejte na webu RRsoft v sekci věnované hře World Of Warcraft!</p>

    <h4>Co všechno zde na webu ke hře World Of Warcraft najdete?</h4>

        <p>World Of Warcraft Addon <a href="/titan-gathered-world-of-warcraft-addon/" >Titan Gathered</a> </p>

    <h3>World Of Warcraft addony:</h3>

        <div>
            <ul>
                {foreach from = $patches_list item = patch key = key}
                <li>
                    <p>Wold Of Warcraft addony ke stažení pro <a href="/world-of-warcraft/addons/patch-{$patch.tag|replace:'.':''}/" >Patch {$patch.tag}</a></p>
                </li>
                {/foreach}
            </ul>
</div>

<h3>WoW Portály</h3>
<ul>
    <li>
    <a href="http://thottbot.com/" ><b>Thottbot</b> - WoW databáze</a>
    </li>
    <li>
<a href="http://wow.allakhazam.com/" ><b>Allakhazam</b> - WoW Addony, makra, scripty, WoW itemy, mapy</a>
    </li>
    <li>
<a href="http://wow.warcry.com/" ><b>Warcry</b> - WoW Portál a kompletní WoW databáze</a>
    </li>
</ul>

    <h3>WoW home linky</h3>
    <ul>
        <li>
<a href="http://eu.wowarmory.com/" ><b>WoW Armory</b> - WoW online realm databáze</a>
        </li>
        <li>
<a href="http://forums.wow-europe.com/" ><b>WoW Forum</b> - Blizzard original forum</a>
        </li>
       <li>
<a href="http://www.wow-europe.com/en/patchnotes/" ><b>WoW Patche</b> - Informace o posledních aktualizacích (Last Patch Notes)</a>
        </li>
    </ul>
</div>