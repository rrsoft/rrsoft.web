<div class="story_box">

    <h1 class="addons-list-title"><a href="/world-of-warcraft/">World Of Warcraft</a> &raquo; Addony</h1>

    <p>Seznam vydaných aktualizací hry World Of Warcraft, pro které byly nalzeny addony.</p>
        <p class="citace"><b>Upozornění!</b> Tento seznam aktualizací obsahuje pouze balíčky s historií addonů!</p>

    <h2>Seznam vydaných aktualizací:</h2>

    {foreach from = $patches_list item = patch key = key}
        <div class="patch_list">
            <ul class="patch_list">
                <li class="patch_list">
                    <div class="left">
                        <span class="patch_id">{$patch.tag}</span>
                    </div>
                    <div class="left caption">
                        <h3>
                            <a href="/world-of-warcraft/" >World Of Warcraft</a> &raquo; Addony &raquo;
                            <a href="/world-of-warcraft/addons/patch-{$patch.tag|replace:'.':''}/" >Patch {$patch.tag}</a>
                        </h3>
                    </div>
                    <div class="patch_list_clear left">
                        <span class="span">Datum: <span class="values">{$patch.released}</span></span>
                        <span class="span">Kompilace: <span class="values">{$patch.version}</span></span>
                        <span class="span">Toc file ID: <span class="values">{$patch.toc}</span></span>
                    </div>
                </li>
            </ul>

        </div>
    {/foreach}
    <div class="clear"></div>

</div>
