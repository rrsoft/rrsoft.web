<div id="mininavigator">
    <ul class="grid5">

        <li class="vmware">
            <h5 class="mininavigator-h3"><a href="/konsolidace-fyzickych-serveru-virtualizace/">Virtualizace
                    serverů</a></h5>

            <p class="perex">
                Konsolidace fyzických serverů a virtualizace.
            </p>
            <a href="/konsolidace-fyzickych-serveru-virtualizace/" class="more">Číst více</a>
        </li>

        <li class="gentoo">
            <h5 class="mininavigator-h3"><a href="/budovani-pocitacovych-siti/">Počítačové sítě LAN</a></h5> 

            <p class="perex">
                Návrh a budování počítačových sítí.
            </p>
            <a href="/budovani-pocitacovych-siti/" class="more">Číst více</a>
        </li>
        <li class="backups">
            <h5 class="mininavigator-h3"><a href="/zalohovani-moderni-virtualizovane-infrastruktury/">Zálohování virtualizace</a></h5> 

            <p class="perex">
                Moderní zálohování virtuálního prostředí.
            </p>
            <a href="/zalohovani-moderni-virtualizovane-infrastruktury/" class="more">Číst více</a>
        </li>
        <li class="ubuntu">
            <h5 class="mininavigator-h3"><a href="/webhosting-hostingove-sluzby/">Hostingové služby</a></h5> 

            <p class="perex">
                Webhostingové služby a pronájem VPS.
            </p>
            <a href="/webhosting-hostingove-sluzby/" class="more">Číst více</a>
        </li>
        <li class="free-webhosting">
            <h5 class="mininavigator-h3"><a href="/free-webhosting-pro-neziskove-organizace/">Free SVN a webhosting</a>
            </h5> 

            <p class="perex">
                Free Webhosting a SVN pro Otevřené projekty.
            </p>
            <a href="/free-webhosting-pro-neziskove-organizace/" class="more">Číst více</a>
        </li>
    </ul>
</div>
