<div id="tripple_box">
    <ul class="grid1">

	<li class="grid1" >
	    <h3><a href="/vyvoj-vebovych-aplikaci/">Programování Webů a aplikací</a></h3>

	    <p class="perex">
            Vytvoříme webové stránky, databázi, nebo desktopovou aplikaci šitou na míru podle individuálních potřeb zákazníka.
	    </p>
        <img src="{$cdn.hostname}/Images/product-left.svg" alt="Výroba webů a webových aplikací" width="242" height="152" />
	</li>

	<li class="grid1">
	    <h3><a href="/graficke-studio-a-graficke-sluzby/">Webdesign a tisková grafika</a></h3>

	    <p class="perex">
            Nabízíme širokou škálu grafických služeb, od webové grafiky, až po výroby letáků, vizitek nebo návrhu billboardu.
	    </p>
        <img src="{$cdn.hostname}/Images/product-middle.svg" alt="Grafické služby a webdesign" width="242" height="152" />
	</li>

	<li class="grid1">
	    <h3><a href="/seo-optimalizace-pro-vyhledavace/">Optimalizace pro vyhledavače</a></h3>

	    <p class="perex">
            Vytvoříme marketingový plán, upravíme strukturu a obsah webových stránek, pro lepší umísťování ve vyhledavačích.
	    </p>
        <img src="{$cdn.hostname}/Images/product-right.svg" alt="Optimalizace pro vyhledavače" width="242" height="152" />
	</li>

    </ul>
</div>
