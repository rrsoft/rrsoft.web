<div id="related_stories">
    {*<h3>Nejčtenější články</h3>*}
    <ul>
        <li class="box">
            <img class="icon" src="{$cdn.hostname}/Images/story_icon1.svg" alt="Related story" width="32" height="32" />
            <h6 class="related-stories-h3"><a href="/navody/instalace-serveru-cvs-na-gentoo-linux/">Instalace serveru CVS na Gentoo linux</a></h6>

        </li>

        <li class="box">
            <img class="icon" src="{$cdn.hostname}/Images/story_icon1.svg" alt="Related story" width="32" height="32" />
            <h6 class="related-stories-h3"><a href="/softwarove-licence/">Stručný výklad softwárových llicencí</a></h6>

        </li>
        <li class="box">
            <img class="icon" src="{$cdn.hostname}/Images/story_icon1.svg" alt="Related story" width="32" height="32" />
            <h6 class="related-stories-h3"><a href="/navody/acer-travelmate-obnova-biosu-po-nezdarne-aktualizaci/">Acer - obnovení mrtvého BIOSu po aktualizaci</a></h6>

        </li>
        <li class="box">
            <img class="icon" src="{$cdn.hostname}/Images/story_icon1.svg" alt="Related story" width="32" height="32" />
            <h6 class="related-stories-h3"><a href="/navody/pripojeni-synology-nas-jako-vzdalene-uloziste-pro-veeam-backup-and-replication/">Připojení Synology NAS jako vzdálené repository</a></h6>

        </li>
        <li class="box">
            <img class="icon" src="{$cdn.hostname}/Images/story_icon1.svg" alt="Related story" width="32" height="32" />
            <h6 class="related-stories-h3"><a href="/navody/postup-instalace-addonu-do-hry-world-of-warcraft/">Podrobný návod Instalace addonů</a></h6>

        </li>
    </ul>
</div>
