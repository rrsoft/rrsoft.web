<div id="hostings">
    <ul class="sepa">

        <li class="single" >
            <h3><a href="/vyvoj-vebovych-aplikaci/">SHARE</a></h3>
            <p class="desc">FREE</p>
            {*<p class="perex">*}
                {*Vytvoříme webové stránky podle individuálních potřeb zákazníka tak, aby splňovaly základní požadavky a tématické potřeby včetně optimalizace SEO.*}
            {*</p>*}
            {*<img src="/Images/product-left.png" alt="Výroba webů a webových aplikací" />*}
            <ul class="inner">
                <li>8GB diskového prostoru</li>
                <li>1x databáze MySQL</li>
                <li>Účet FTP</li>
                {*<li>1x dedikovaná IP</li>*}
                {*<li>Neomezený přenos dat</li>*}
            </ul>
        </li>

        <li class="single">
            <h3><a href="/graficke-studio-a-graficke-sluzby/">GLOBAL</a></h3>
            <p class="desc">99 Kč / měsíc</p>

            <ul class="inner">
                <li>30GB diskového prostoru</li>
                <li>1 IP adresa</li>
                <li>Neomezený přenos dat</li>
                <li>Sesured FTP</li>
                <li>5x databáze MySQL</li>
                <li>Snapshot backup</li>
            </ul>
            {*<p class="perex">*}
                {*Nabízíme širokou škálu grafických služeb, jako je výroba webové grafiky, redesign stávajícího webu, výroba tiskovin, loga a logotypy, letáky, billboardy a další služby.*}
            {*</p>*}
            {*<img src="/Images/product-middle.png" alt="Grafické služby a webdesign" />*}
        </li>

        <li class="single">
            <h3><a href="/seo-optimalizace-pro-vyhledavace/">PROFI</a></h3>
            <p class="desc">199 Kč / měsíc</p>

            <ul class="inner">
                <li>Neomezený diskový prostor</li>
                <li>3 dedikované IP</li>
                <li>Neomezený přenos dat</li>
                <li>Sesured FTP</li>
                <li>SSH přístup</li>
                <li>Podpora 24 / 7</li>
                <li>Neomezená databáze MySQL</li>
                <li>Snapshot backup</li>

            </ul>
            {*<p class="perex">*}
                {*Upravujeme struktura a obsah webových stránek tak, aby splňoval všechna kritéria a standardy pro lepší umísťování ve většině vyhledavačů.*}
            {*</p>*}
            {*<img src="/Images/product-right.png" alt="Optimalizace pro vyhledavače" />*}
        </li>

    </ul>
</div>
