<div class="story_box">

    <div class="main-text">
        <div>
            <h1>soubory ke stažení</h1>
            <p>Ještě před tím než začnete stahovat jaký-koli sofware umístěný na této stránce,
                přečtěte si prosím několik důležitých informací o
                <a href="/softwarove-licence/" >sofwarových licencích</a> vysvětlující podmínky
                legálního užívání a dalšího šíření tohoto software...
            </p>

        </div>
        <div class="download_div">
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <!-- siroky_text -->
            <ins class="adsbygoogle"
                 style="display:inline-block;width:468px;height:60px"
                 data-ad-client="ca-pub-9949694151182599"
                 data-ad-slot="8873382466"></ins>
            <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
        </div>

        <div class="download_div">
            <h2>Ventrilo v2.3.0 &nbsp;<span class="span">Licence:</span><span class="span">Freeware</span>
                </h2>

            <p>Program Ventrilo slouží k přenosu hlasu po internetu (VoIP).
                Podle mého názoru a mých zkušeností, je to jeden z nejlepších programů zabývající se přenosem hlasu po internetu.
                Je jednoduchý, přehledný a není přeplácaný zbytečnostmi po stránce administrační ani uživatelské.</p>

            <strong>Výrobce:</strong> <a href="http://www.ventrilo.com/" >www.ventrilo.com</a>
            <a href="/downloads/ventrilo-2.3.0-Windows-i386.exe" class="download right"></a>

        </div>
        <div class="download_div">
            <h2>Balíček addonů - Medyn's pack&nbsp;&nbsp;
                 <span class="span">{$AddOnsPack}&nbsp;&nbsp;<i class="span">(27.02.2008)</i></span></h2>

            <p>Balíček obsahuje nejpoužívanějších addony do hry World Of Warcraft (WoW-BC).</p>
            <p>Addony jsou kompatibilní s posledním datadiskem BC, patche 2.3.3 a obsahují několik opravdu populárních addonů jako je
                Auctioneer, Metamap, Bartender3, kompletní kolekci addonů Titan nebo Xperl.</p>
            <p>Podrobnější popis většiny addonů z tohoto balíčku naleznete v sekci <a href="/world-of-warcraft/addons/" >Addony do World Of Warcraft</a>,
                kde je možné stahnout si každý zvlášť podle vlastního vkusu a potřeb.</p>

            <a href="/downloads/addonspack-27022008.zip" class="download right"></a>
        </div>
        <div class="download_div">
            <h2>Quest Helper v0.49 (WoW 2.4.3)&nbsp;&nbsp;
                 <span class="span">{$qh}&nbsp;&nbsp;<i>(4.06.2008)</i></span></h2>

            <p>Populární addon Quest helper pro verzi World Of Warcraft 2.3.3.</p>
            <p>Tato verze by měla spolehlivě fungovat na verzích WoW před patchem 2.4.2</p>

            <a href="/downloads/quest-helper-0.49.zip" class="download right"></a>
        </div>
        <div class="download_div">
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <!-- siroky_text -->
        <ins class="adsbygoogle"
             style="display:inline-block;width:468px;height:60px"
             data-ad-client="ca-pub-9949694151182599"
             data-ad-slot="8873382466"></ins>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
        </div>
        </div>
    </div>
