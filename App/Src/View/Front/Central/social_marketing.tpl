<div id="social_marketing">
    <img class="soc-image" src="{$cdn.hostname}/Images/social_network.png" alt="Využití socíální sítě jako zdroj internetového marketingu" width="295"/>
    <div class="left-box">

        <h2 class="leftdd">Sociální sítě jako zdroj internetového marketingu</h2>
        <h3><a href="/budovani-socialnich-siti-jako-marketingovy-zdroj/">Co jsou to sociální sítě a jak se dají využít pro internetový marketing?</a></h3>

    </div>
    <p class="citace">"Sociální sítě jsou struktury tvořené jednotlivými uzly, navzájem vědomě či nevědomě spojené společnými atributy."</p>
    {*<a href="/budovani-socialnich-siti-jako-marketingovy-zdroj/" ><h2 class="left">Sociální síť je hlavní zdroj internetového marketingu</h2></a>*}
    {*<a href="/budovani-socialnich-siti-jako-marketingovy-zdroj/" ><h2 class="left">Sociální sítě jako nástroj internetového marketingu</h2></a>*}
    <div class="vmware-right">
        {*<p class="citace">Sítě jsou sociální struktury tvořené uzly vědomě či nevědomě navzájem spojené společnými zájmy nebo atributy.</p>*}

        <p>
            Budování sociálních sítí podporuje rozvoj vztahů mezi lidmi nebo komunitami lidí se stejným tématickým obsahem a většinou fungují jako velmi dobrý zdroj informací s cílenou tématikou.
        </p>
        <p>Proto se dají velmi dobře využít jako zdroj zvýšení návštěvnosti webových stránek, zacílení reklamních kampaní a nebo během procesu <a href="/seo-optimalizace-pro-vyhledavace/">optimalizace webových stránek pro vyhledavače.</a> </p>
    </div>
</div>
