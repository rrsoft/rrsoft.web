<div id="webhosting-splash">
    <ul class="grid1">

        <li class="grid1" >
            <h2><a href="/graficke-studio-a-graficke-sluzby/">1. Registrace domény</a></h2>

            <p class="perex">
                {*Pomůžeme vám s návrhem loga (logotypu) – celé logo navrhneme a zpracujeme pro web, tisk nebo ho pouze upravíme či zlepšíme jeho kvalitu.*}
                Nemáte pro svůj web žádnou doménu? Nevadí, postaráme se o registraci nové domény a zajistíme zdarma správu a potřebná nastavení!
            </p>
            <a href="" class="icon-arrow icon-arrow-down"></a>
        </li>

        <li class="grid1">
            <h2><a href="/graficke-studio-a-graficke-sluzby/">2. Vytvoříme Web</a></h2>

            <p class="perex">
                {*Pomůžeme vám s návrhem loga (logotypu) – celé logo navrhneme a zpracujeme pro web, tisk nebo ho pouze upravíme či zlepšíme jeho kvalitu.*}
                Jako další krok na základě vašich požadavků vytvoříme webové stránky a nebo webovou prezentaci, včetně loga nebo logotypu pro tisk!
            </p>
            <a href="" class="icon-arrow icon-arrow-right"></a>
        </li>

        <li class="grid1">
            <h2><a href="/seo-optimalizace-pro-vyhledavace/">3. Webhosting zdarma</a></h2>

            <p class="perex">
                Web je hotový ale kam s ním? Nabízíme webhosting pro námi vytvořený web zdarma a nebo superhosting za nejvýhodnější ceny!
            </p>
            <a href="" class="icon-arrow icon-arrow-up"></a>
        </li>

    </ul>
</div>
