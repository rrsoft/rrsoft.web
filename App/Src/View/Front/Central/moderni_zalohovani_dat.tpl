<div id="modern_backup">
    <div class="left-box">
        <h2 class="leftdd">Zálohování virtuálního prostředí</h2>
    </div>
    <div class="vmware-right">
        <h3><a href="/zalohovani-moderni-virtualizovane-infrastruktury/">Moderní nástroje zálohování</a></h3>
        <p>
            Sada <a href="http://go.veeam.com/download-availability-suite-v8.html" >Veeam Availability Suite™</a> je nátroj pro zálohování, obnovení a replikace v prostředí <a href="http://www.vmware.com/cz">VMware</a> a <a href="http://blogs.technet.com/b/technetczsk/p/microsoft-hyper-v.aspx">Hyper-V</a>.</p>

        <p class="schovat-768">
            Součástí sady je i produkt <a href="http://go.veeam.com/virtualization-management-one-solution-v8">Veeam ONE™</a> pro monitorování, reportování a plánování VM.
        </p>

    </div>
    <img class="soc-image" src="{$cdn.hostname}/Images/backup_services.svg" width="275" height="155" alt="Moderní zálohování ve virtuálním prostředí" />

</div>
