<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs"> 
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="Content-language" content="cs" />
        <meta name="Description"  content="Různé pohledy na zdravotní problémy " />
        <title>Registrace</title>
    </head>
    <body>
        <h2>Děkujeme za registraci.</h2>
        <p>Tento ativační email zaslal automat, proto na něj neodpovídejte a reghistrační idaje pečlivě uschovejte.</p>
        <h4>Přihlašovací informace:</h4>
        <p>
        ----------------------------------<br />
        Uživatel: {$auth_registration_email_confirm.post.t_user}<br />
        Heslo: {$auth_registration_email_confirm.post.t_pass}<br />
        ----------------------------------<br />
        </p>
        <p>
				Účet je nutné před použitím aktivovat. Použijte prosím aktivační link uvedený níže.
        </p>
        <p>
            <a href="{$auth_registration_email_confirm.host}/registrace/{$auth_registration_email_confirm.key}" >{$auth_registration_email_confirm.host}/registrace/{$auth_registration_email_confirm.key}</a>
        </p>

        <p>S pozdravem</p>
        <p>Sytem administrátor</p>
    </body>
</html>