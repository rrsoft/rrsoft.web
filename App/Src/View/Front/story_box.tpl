
<!-- START STORY BOX -->
<div class="story_box">
    {if $article.title}
        <h1>{$article.title}</h1>
    {/if}
    {if $site_id > 1}
        <!-- START TOP SOCIAL PANEL IMPLEMENTATION -->
        {*<div id="social-panel">*}
            {*<div*}
                    {*class="fb-share-button"*}
                    {*data-share="true"*}
                    {*data-href="http://www.rrsoft.cz{$url}"*}
                    {*data-layout="button_count"*}
                    {*data-show-faces="true">*}
            {*</div>*}
            {*<div class="g-plus" data-action="share" data-annotation="bubble"*}
                 {*data-href="http://www.rrsoft.cz{$url}"></div>*}
            {*<a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.rrsoft.cz{$url}"*}
               {*data-via="rrsoftcz" data-related="rrsoft">Tweet</a>*}
            {*{if $article.id}*}
                {*<span class="span right">Publikováno: {$article.published|date_format},&nbsp;*}
                {*<span style="float: right">*}
                    {*Autor: <a href="https://www.google.com/+RadekRoza?rel=author">{$article.author}</a>*}
                {*</span>*}
    {*</span>*}
            {*{/if}*}
        {*</div>*}
        <!-- END TOP SOCIAL PANEL IMPLEMENTATION -->

        <!-- START TOP SOCIAL PANEL IMPLEMENTATION -->
        <div id="social-panel">
            {if $article.id}
                <span class="span right">Publikováno: {$article.published|date_format},&nbsp;
                <span style="float: right">
                    Autor: <a href="https://www.google.com/+RadekRoza?rel=author">{$article.author}</a>
                </span></span>
            {/if}
        </div>
        <!-- END TOP SOCIAL PANEL IMPLEMENTATION -->

    {/if}
    <!-- story box content start -->
    {$story_box_content}
    <!-- story box content end -->

    <!-- story box footer info start -->
    {if $article.id}
        <span class="story_info">
        Shlédnuto: {$article.views}, Zdroj: <a href="http://{$article.source}">{$article.source}</a> <span
                    style="float: right"> Autor: <a
                        href="https://www.google.com/+RadekRoza?rel=author">{$article.author}</a></span>
    </span>
    {/if}
    <!-- story box footer info end -->

    <!-- start facebook comment plugin -->
    <p>&nbsp;</p>

    <div class="fb-comments" data-href="http://www.rrsoft.cz{$url}" data-width="100%" data-numposts="5"
         data-colorscheme="light"></div>
    <!-- end facebook comment plugin -->
</div>
<!-- END STORY BOX -->
