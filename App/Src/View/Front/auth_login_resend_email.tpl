<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs"> 
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="Content-language" content="cs" />
		<meta name="Description"  content="Různé pohledy na zdravotní problémy " />
		<title>Reaktivační email</title>
	</head>
	<body>
		<div>
			<h2>Reaktivační email</h2>

			<p>
				Použijte prosím níže uvedený link k aktivaci účtu <strong>{$auth_login_resend_email.post.t_resend_username}</strong>
			</p>
			<p>
				<a href="{$auth_login_resend_email.host}/registrace/{$auth_login_resend_email.key}" >{$auth_login_resend_email.host}/registrace/{$auth_login_resend_email.key}</a>
			</p>
			<p>
				S pozdravem
			</p>
			<p>menimepodnikani.cz</p>
		</div>
	</body>
</html>