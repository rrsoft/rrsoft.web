<div id="story_box" >
	<form ID="user_profil" name="profil" action="/profil/" method="post">
	    <div>
					<h1>Uživatelský profil</h1>
					<p>Zde můžete změnit některé informace vašeho profilu.</p>
					{if $auth_profil.errors}
						<p class="red">{$auth_profil.errors}</p>
					{/if}
					{if $auth_profil.messages}
						<p>{$auth_profil.messages}</p>
					{/if}
	    </div>
		<table class="left">
		    <tr style="height: 54px;">
				<td class="label">Uživatelské jméno:</td>
				<td class="need">&nbsp;</td>
				<td class="input">
				    <span class="big">{$auth_profil.post.username}</span>
					<input type="hidden" name="username" value="{$auth_profil.post.username}" />
					<input type="hidden" name="user_id" value="{$auth_profil.post.user_id}" />
				</td>
			</tr>
			<tr>
				<td class="label">Jméno:</td>
				<td class="need">&nbsp;</td>
				<td class="input"><input type="text" name="firstname" size="30" value="{$auth_profil.post.firstname}" /></td>
			</tr>
			<tr>
				<td class="label">Příjmení:</td>
				<td class="need">&nbsp;</td>
				<td class="input"><input type="text" name="lastname" size="30" value="{$auth_profil.post.lastname}" /></td>
			</tr>
			<tr>
				<td class="label">Email:</td>
				<td class="need">&nbsp;</td>
				<td class="input"><input type="text" name="email" size="30" value="{$auth_profil.post.email}" /></td>
			</tr>
			<tr>
				<td colspan="3">
{*					<hr>*}
{*					<input type="image" src="/Images/button.png" id="submit" /><input type="hidden" name="b_update" value="1" />
*}				</td>
			</tr>
		</table>
					<table class="right">
			<tr>
				<td class="label">Datum narození:</td>
				<td class="need">&nbsp;</td>
				<td class="input">
					{html_select_date start_year='-60' month_format='%m' field_order='DMY' time=$auth_profil.post.age}
				</td>
			</tr>
			<tr>
				<td class="label">Pohlaví</td>
				<td class="need">&nbsp;</td>
				<td class="input">
					{html_radios name='sex' options=$auth_profil.sex selected=$auth_profil.post.sex separator='&nbsp;'}
				</td>
			</tr>
			<tr>
				<td class="label">Adresa:</td>
				<td class="need">&nbsp;</td>
				<td class="input"><input type="text" name="address" size="30" value="{$auth_profil.post.address}" /></td>
			</tr>
			<tr>
				<td class="label">Město:</td>
				<td class="need">&nbsp;</td>
				<td class="input"><input type="text" name="city" size="30" value="{$auth_profil.post.city}" /></td>
			</tr>
			<tr>
				<td class="label">PSČ:</td>
				<td class="need">&nbsp;</td>
				<td class="input"><input type="text" name="post" size="30" value="{$auth_profil.post.post}" /></td>
			</tr>
			<tr>
				<td class="label">Země:</td>
				<td class="need">&nbsp;</td>
				<td class="input">
					<select name="country" style="width: 215px;">
						<option value="0">-- vyberte zemi --</option>
						{html_options options=$auth_profil.landcodes selected=$auth_profil.post.country}
					</select>

				</td>
			</tr>
		</table>
					<div class="line">
					<input type="image" src="/Images/save.png" id="submit" /><input type="hidden" name="b_update" value="1" />
					</div>
	</form>
</div>