
<!-- START CONTACTS BOX -->
<div id="contacts">
    <h1>Radek Roža</h1>
    <div class="small">RRSOFT - eStudio</div>

    <div class="right">
        <img class="thumb" src="{$cdn.hostname}/Images/Me.png" alt="Radek Roža - RRSOFT" width="240" />

        <h2>Proč si vybrat nás?</h2>
        <p class="citace">Od ostatních nás odlišuje naše striktní filozofie a způsob řešení všech požadavků zákazníka.
            Jsme kvalifikovaní, progresivní, efektivní a máme více jak 20 let zkušeností.</p>
        <p class="citace">
            <br />Naše práce nespočívá v soupeření s konkurencí, nýbrž v prosazování qvality s využitím integrované sady dovedností a znalostí.
        </p>
    </div>
    <div class="left-panel">

       <br />

        <h2>Adresa a sídlo firmy</h2>
        <ul>
            <li>Purkyňova 26, 391 02, Tábor</li>
            <li>IČO: 655 83 990</li>
            <li>Banka: GE-Money: 158739681/0600</li>
            <li>Email: <a href="mailto:roza@rrsoft.cz" >radek.roza(zavináč)rrsoft.cz</a></li>
            <li>Jabber: bajtlamer(zavináč)gmail.com</li>
            <li>Skype: rrsoft.cz</li>
            <li>GSM T-mobile: +420 733 378 437</li>
        </ul>

        <h2 class="sluzby-regiony" c>Služby nabízíme v regionech:</h2>
        <ul class="lokality">
            <li>Karlovy Vary</li>
            <li>Praha</li>
            <li>Tábor</li>
            <li>České Budějovice</li>
        </ul>

        <div id="contact-icons">
            <ul >
                <li><a class="social_buttons_google" href="https://www.google.com/+RrsoftCz"></a></li>
                <li><a class="social_buttons_twitter" href="https://twitter.com/rrsoftcz"></a></li>
                <li><a class="social_buttons_facebook" href="https://www.facebook.com/rrsoft.cz"></a></li>
                <li><a class="social_buttons_in" href="https://www.linkedin.com/profile/view?id=112752354"></a></li>
            </ul>
        </div>
    </div>
</div>
<!-- END CONTACTS BOX -->