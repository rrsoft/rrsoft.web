<div id="error-404">
    <!--<img src="/Images/404.png" alt="Error 404 Not found" height="155"  width="155" />-->
    <h1>Chyba 404 - Stránka nenalezena</h1>
    <p>&nbsp;</p>
    <h4>Omlouváme se, ale hledaný odkaz, článek, nebo cílová stránka nebyly nalezeny.</h4>
    <p>&nbsp;</p>
    <h4>Možné příčiny:</h4>
    <ul>
        <li>Článek nebo obsah stránky byl smazán nebo zakazán administrátorem</li>
        <li>Stránky nebo zdroje odkazu byly odstraněny</li>
        <li>ID článku neexistuje</li>
    </ul>
</div>
