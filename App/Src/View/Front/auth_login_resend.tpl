<div id="story_box" >
    <form ID="user_email_resent" name="resend" action="/login/?resent=1" method="post">
	<div>
	    <h1>Odeslání aktivačního emailu</h1>
	    <p>Zadejte prosím email účtu, pro který si přejete zaslat aktivační email.</p>

	    {if $auth_login_resend.errors}
		<p class="red">{$auth_login_resend.errors}</p>
	    {/if}
	    {if $auth_login_resend.msg}
		<p>{$auth_login_resend.msg}</p>
	    {/if}
	    <p>&nbsp;</p>
	</div>

	<div class="mail_resend">
	    Zadejde email:&nbsp;<input type="text" name="t_resend_email" size="30" value="{$auth_login_resend.post.t_resend_email}" />

	</div>
	<div><input type="image" src="/Images/send.png" id="submit" /><input type="hidden" name="b_resent" value="1" /></div>
    </form>
</div>