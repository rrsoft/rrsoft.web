<div id="story_box" >
	<form ID="user_registration" name="registration" action="/registrace/" method="post">
		<table id="form">
			<tr>
				<td colspan="3">
					<h1>Nová registrace</h1>
					<i>Zde můžete založit nový uživatelský účet. Položky označené hvězdičkou nesmí být prázdné.</i>
					<p>&nbsp;</p>
					{if $auth_registration.errors}
						<p class="red">{$auth_registration.errors}</p>
					{/if}
					{if $auth_registration.errors }
						<p>{$auth_registration.messages}</p>
					{/if}
				</td>
			</tr>
			<tr>
				<td class="label">Uživatelské jméno:</td>
				<td class="need"> * </td>
				<td class="input"><input type="text" name="t_user" size="30" value="{$auth_registration.post.t_user}" /></td>
			</tr>
			<tr>
				<td class="label">Email:</td>
				<td class="need"> * </td>
				<td class="input"><input type="text" name="t_email" size="30" value="{$auth_registration.post.t_email}" /></td>
			</tr>
			<tr>
				<td class="label">Heslo:</td>
				<td class="need"> * </td>
				<td class="input"><input type="password" name="t_pass" size="30" value="{$auth_registration.post.t_pass}" /></td>
			</tr>
			<tr>
				<td class="label">Heslo (ověření):</td>
				<td class="need"> * </td>
				<td class="input"><input type="password" name="t_repass" size="30" value="{$auth_registration.post.t_repass}" /></td>
			</tr>
			<tr>
				<td colspan="3"><input type="image" src="/Images/button.png" id="submit" /><input type="hidden" name="b_register" value="1" /></td>
			</tr>
		</table>
	</form>
</div>