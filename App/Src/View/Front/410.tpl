<div id="error-404">
    <h1>Chyba 410 - URL nebo odkaz byl trvale odstraněn</h1>
    <p>&nbsp;</p>
    <h4>Omlouváme se, ale hledaný odkaz byl trvale odstraněn ze systému.</h4>
    <p>&nbsp;</p>
    <h4>Možné příčiny:</h4>
    <ul>
        <li>Článek nebo obsah stránky byl smazán nebo zakazán administrátorem</li>
        <li>Stránky nebo zdroje odkazu byly trvale odstraněny</li>
        <li>Odkazovaný článek již neexistuje</li>
    </ul>
</div>
