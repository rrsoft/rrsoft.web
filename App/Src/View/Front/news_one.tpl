<div id="news_one">
    <ul id="news_body">

	<li class="news_item one" >
	    <h3><a href="/okna/">Výroba oken</a></h3>

	    <p class="perex">
		Okna jsou důležitým měřítkem útulnosti domu. Spojují interiér a exteriér, vnášejí sluneční svit do každého prostoru a umožňují nerušený pohled do přírody.
		<a href="/okna/drevena/" class="more">&raquo;</a>{$news_one}
	    </p>
	</li>

	<li class="news_item two">
	    <h3><a href="/vchodove-dvere/">Vchodové dveře</a></h3>

	    <p class="perex">
		Vchodové dveře určují první dojem domu. Perfektně doplňují architekturu domu a jsou – podle požadovaného efektu – v designu nenápadné, nebo nápadné.
		<a href="/vchodove-dvere/masiv/" class="more">&raquo;</a>
	    </p>
	</li>

	<li class="news_item three">
	    <h3><a href="/protislunecni-ochrana-rolety-a-zaluzie/">Rolety a žaluzie</a></h3>

	    <p class="perex">
		Okenice a posouvací okenice jsou atraktivním doplňkem a nabízejí nejen sluneční ochranu. Jsou nabízeny v různých barvách,  aby ladily s fasádou domu.
		<a href="/protislunecni-ochrana-rolety-a-zaluzie/roletove-systemy/" class="more">&raquo;</a>
	    </p>
	</li>

	<li class="clear">&nbsp;</li>

    </ul>
</div>
