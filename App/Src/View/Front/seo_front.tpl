<div id="seo-front">
    <div id="seo-content">
        <h2 class="slider">Otimalizace pro vyhledavače</h2>
        <p>Optimalizace pro vyhledavače (SEO) není žádné tajemství, vyžaduje jen čistotu, trpělivost a hlavně čas...</p>
        <p><b>Nabízíme:</b></p>
        <ul>
            <li class="slide1">analýzu stávajících stránek</li>
            <li class="slide1">vypracujeme návr a plán optimalizace</li>
            <li class="slide1">přepracování celého webu a nebo přesunutí na nový systém</li>
            <li class="slide1">sledování optimalizovaného webu a provádění pravidelných analýz po dobu minimálně jednoho roku</li>
        </ul>
        <p>Více na stránce <a href="/webdesign-seo/" >Optimalizace SEO</a>.</p>
        <p>We love seeing music live, when people get together and pour out lyrics over chords into a room full of strangers. There’s something essential in four walls that makes music sound so good. Our space is inspired by that idea. It’s not too fancy: a single acoustically treated live room with lots of space to play, a team of people that live for sound, and the tools to make silent smiles.

We’ve also been lucky to record some great artists. Their talent makes this whole thing worthwhile.

We’re accepting new projects.

Hours are from noon to 12am Monday-Saturday, 6pm-12pm Sunday. You can find more information on rates here, give us a call, or schedule a time to come over and take a tour of the studio.</p>
    </div>
</div>
