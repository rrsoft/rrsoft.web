<div id="login_box">
    <form id="user_login_form" name="login" action="/login/" method="POST">

        <h1>{$auth_login.AUTH_LOGIN_FORM_TITLE}</h1>

        <h3>{$auth_login.AUTH_LOGIN_FORM_LABEL}</h3>

        <div class="infozona">
        {if $auth_login.errors}
            <h3 class="red">{$auth_login.errors}</h3>
        {/if}
        {if $auth_login.errors}
            <h3>{$auth_login.messages}</h3>
        {/if}
        </div>
        <div class="label">
            {$auth_login.AUTH_USERNAME}
        </div>
        <div class="input">
            <input type="text" name="t_user" size="30" value="{$auth_login.post.t_user}"/>
        </div>
        <div class="label">
            {$auth_login.AUTH_PASSWORD}
        </div>
        <div class="input">
            <input type="password" name="t_pass" size="30" value="{$auth_login.post.t_pass}"/>
        </div>
        {*<div class="clear"></div>*}
        <div class="buttonzona">
            <a href="#" id="submit"/>Login</a>
            <input type="hidden" name="b_login" value="1"/>
        </div>

    </form>
</div>
