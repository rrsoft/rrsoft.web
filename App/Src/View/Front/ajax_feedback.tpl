        <div class="grid_10">
            <h4>Get an Instant Quote! Receive your lowest quote TODAY.</h4>
            <h6>Information needed: </h6>
            <div id="result"></div>
            <form id="contact-form" action="/" method="post">
                <div class="success" style="display: none; "> Contact form submitted! We will be in touch soon.</div>
                <fieldset>
                    <label class="name">
                        <input type="text" value="Your Name:" id="fullname" onfocus="javascript: if(this.value == 'Your Name:'){
			    this.value = '';
			}" onblur="javascript: if(this.value==''){
			    this.value='Your Name:';
			}" />
                        <span class="error" style="display: none; " id="err_fullname">*This is not a valid name.</span>
                    </label>
                    <label class="phone">
                        <input type="text" value="Telephone:" id="phone" onfocus="javascript: if(this.value == 'Telephone:'){
			    this.value = '';
			}" onblur="javascript: if(this.value==''){
			    this.value='Telephone:';
			}" />
                        <span class="error" style="display: none; " id="err_phone">*This is not a valid phone number.</span>
                    </label>
                    <label class="email">
                        <input type="text" value="E-mail:" id="mailto" onfocus="javascript: if(this.value == 'E-mail:'){
			    this.value = '';
			}" onblur="javascript: if(this.value==''){
			    this.value='E-mail:';
			}" />
                        <span class="error" style="display: none; " id="err_mailto">*This is not a valid email address.</span>
                    </label>
                    <label class="name">
                        <select name = "type" id="type">
                          <option value="none">--Select--</option>
                          <option value="Woven">Woven</option>
                          <option value="Printed">Printed</option>
                        </select>                    
                        <span class="error" style="display: none; " id="err_type">*Select a valid item please.</span>
                    </label>
                    <label class="name">
                        <select name = "type" id="style">
                          <option value="none">--Select--</option>
                          <option value="Straight cut">Straight cut</option>
                          <option value="Mid-fold">Mid-fold</option>
                          <option value="End-fold">End-fold</option>
                        </select>                    
                        <span class="error" style="display: none; " id="err_style">*Select a valid item please.</span>
                    </label>
                    <label class="name">
                        <input type="text" value="Quantity:" id="quantity" onfocus="javascript: if(this.value == 'Quantity:'){
			    this.value = '';
			}" onblur="javascript: if(this.value==''){
			    this.value='Quantity:';
			}" />
                        <span class="error" style="display: none; " id="err_quantity">*This item can't be empty.</span>
                    </label>
                    <label class="name">
                        <input type="text" value="Rush or Regular Delivery?" id="delivery" onfocus="javascript: if(this.value == 'Rush or Regular Delivery?'){
			    this.value = '';
			}" onblur="javascript: if(this.value==''){
			    this.value='Rush or Regular Delivery?';
			}" />
                        <span class="error" style="display: none; " id="err_delivery">*This item can't be empty.</span>
                    </label>
                    <label class="message">
                        <textarea id="message" onfocus="javascript: if(this.value == 'Message:'){
			    this.value = '';
			}" onblur="javascript: if(this.value==''){
			    this.value='Message:';
			}" />Message:</textarea>
                        <span class="error" style="display: none; " id="err_message">*The message is too short.</span>
                    </label>
                    <span class="btns">
                        <a class="button" data-type="submit" id="submit">Submit</a>
                    </span>
                </fieldset>
            </form>
        </div>
