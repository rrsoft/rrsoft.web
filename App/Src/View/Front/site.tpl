<!DOCTYPE html>
<html lang="cs">

<head>
    <meta name="verify-v1" content="25xszQZXWRlB9r320VmJykXEyB94yAdMmHRwgQU3sJg="/>
    <meta name="msvalidate.01" content="2C410502F7EB69DFD05956926EEECB49"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="Content-language" content="cs"/>
    <meta name="Keywords" content="{$meta.keywords}"/>
    <meta name="Description" content="{$meta.description}"/>
    <meta name="resource-type" content="document"/>
    <meta name="rating" content="General"/>
    <meta name="author" content="Radek Roža - RRsoft"/>
    <meta name="copyright" content="Copyright © 2015 RRsoft, All rigts reserverd. Email:  office(at)rrsoft.cz"/>
    <meta name="robots" content="index,follow"/>
    <meta http-equiv="Imagetoolbar" content="no"/>
    <meta http-equiv="Cache-control" content="no-cache"/>
    <meta name="theme-color" content="#2894d5"/>
    <link rel="author" href="https://www.google.com/+RadekRoza"/>
    <link rel="StyleSheet" type="text/css" href="{$cdn.hostname}/Css/default-min.css"/>
	<link rel="manifest" href="/manifest.json">
	
    {if $site_id === 1}
        <link rel="StyleSheet" type="text/css" href="{$cdn.hostname}/Css/slider-min.css" media="screen"/>
    {/if}

    {*<link rel="StyleSheet" type="text/css" href="{$cdn.hostname}/Css/colorbox-min.css" media="screen"/>*}
    {*<link rel="StyleSheet" type="text/css" href="{$cdn.hostname}/Css/fonts.css" media="screen"/>*}
    <link rel="StyleSheet" type="text/css" href="{$cdn.hostname}/Css/responsive-min.css" media="screen and (max-width:960px)"/>
    {*<link rel="StyleSheet" type="text/css" href="{$cdn.hostname}/Css/Responsive.css" media="screen and (max-width:960px)"/>*}
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>{$meta.title}</title>
    <link rel="icon" type="image/png" href="{$cdn.hostname}/Images/favicon.ico">
</head>
<body>
<div id="top-navigator">
    <div class="inner">
        <ul class="left">
            <li class="home-ico"><a class="home-ico" href="/">Home</a></li>
        </ul>
        <ul class="right">
            <li><span class="phone-ico">Podpora 24 / 7: +420 733 378 437</span></li>
            <li><a class="mail-ico" href="/">support@rrsoft.cz</a></li>
            <li class="last"><a class="login-ico" href="/login/">Přihlásit se</a></li>
            {*<li class="last"><a class="login-ico" href="/admin/">Admin</a></li>*}
            <li><a class="contact-ico" href="/kontakty/">Kontakty</a></li>
        </ul>
    </div>
</div>
<div id="main_wrap">
    <!--STAR MAIN MENU-->
    {if $device_type > 1}
    <div id="menu">
        <ul>
            {foreach $menu as $mItemitem}
                <li>
                    <a href="{$mItemitem.url}"
                       {if $mItemitem.selected}class="{$mItemitem.selected}{if !$smarty.foreach.menu.last} last-menu-item{/if}"{/if}>
                        <span class="menuSpan">{$mItemitem.name}<br/><span class="subli">{$mItemitem.description}</span></span>
                    </a>

                </li>
            {/foreach}
        </ul>
    </div>
    {/if}
    <div id="header">
        {$header}
    </div>
    <!--END MAIN MENU-->
    <div id="main">
        <!--START UNDERHEADER SPACER-->
        <div id="underheader{$html_surfix}"></div>
        <!--END UNDERHEADER SPACER-->
        <!-- START CENTRAL BOX -->
        <div id="central_box{$html_surfix}">
            {$central}
        </div>
        <!-- END CENTRAL BOX -->
        <!-- START RIGHT BOX -->
        <div id="right_box{$html_surfix}">{$right_box}&nbsp;</div>
        <!-- END RIGHT BOX -->

        <!-- START STORY BOX FOOTER -->
        <div class="clear"></div>
        {if !$html_surfix}
            <div id="central_box_footer"><a class="icon-up" href="#top-navigator" alt="Navigovat nahoru.."
                                            title="Přejít nahoru"></a></div>
        {/if}
        <!-- END STORY BOX FOOTER -->
        <div class="clear"></div>
    </div>
</div>
<!--START FOOTER-->
<div id="footer-shadow">
    <div class="clear"></div>
    <div id="footer-bottom">

        <ul class="footer-bottom-ul">
            <li class="header"><h3>Služby</h3></li>
            <li><a class="fico" href="/konsolidace-fyzickych-serveru-virtualizace/">Virtualizace fyzických serverů</a>
            </li>
            <li><a class="fico" href="/budovani-pocitacovych-siti/">Budování počítačových sítí</a></li>
            <li><a class="fico" href="/graficke-studio-a-graficke-sluzby/">Webdesign a Tisková grafika</a></li>


        </ul>
        <ul class="footer-bottom-ul">
            <li class="header"><h3>Články</h3></li>
            <li><a class="fico" href="/navody/instalace-serveru-cvs-na-gentoo-linux/">Instalace systému CVS</a></li>
            <li><a class="fico" href="/zalohovani-moderni-virtualizovane-infrastruktury/">Moderní zálohování dat</a>
            </li>
            <li><a class="fico" href="/budovani-socialnich-siti-jako-marketingovy-zdroj/">Sociální sítě a marketing</a>
            </li>
        </ul>
        <ul class="footer-bottom-ul">
            <li class="header"><h3>Partneři</h3></li>
            <li><a class="fico" href="http://www.eclair.cz/" title="Eclair.cz s.r.o. - Výroba webových stránek">Webové
                    stránky a eShopy</a></li>
            <li><a class="fico" href="http://www.valid.cz" title="Valid.cz - Veeam Technical sales Professional">Veeam
                    Silver Pro Partner</a></li>
            <li><a class="fico" href="http://www.nwtcomp.cz" title="NWT Computer s.r.o. - Tábor">Dodavatel serverů DELL
                    &amp; HP</a></li>
        </ul>

        <div class="footer-right">
            <h3 class="h3-footer"><span>Hostingové</span> lokality</h3>

            <p>USA FL - Orlando</p>

            <p>USA VA - Richmond</p>

            <p>USA CA - Los Angeles</p>

            <h4><span>Evropa</span></h4>

            <p>Irsko - Dublin</p>

            <p>Holandsko - Amsterdam</p>

            <p>Česká republika - Praha</p>

            <div id="social-panel-footer">
                <ul>
                    <li><a class="social_buttons_google" href="https://www.google.com/+RrsoftCz"></a></li>
                    <li><a class="social_buttons_twitter" href="https://twitter.com/rrsoftcz"></a></li>
                    <li><a class="social_buttons_facebook" href="https://www.facebook.com/rrsoft.cz"></a></li>
                    <li><a class="social_buttons_in" href="https://www.linkedin.com/profile/view?id=112752354"></a></li>
                </ul>
            </div>

        </div>

        <div id="footer-phone-box">
            <ul class="apps-buttons">
                <li><a class="apps-mysql" href="http://www.mysql.com"
                       title="MySQL - most popular open source database"></a></li>
                <li><a class="apps-php" href="http://php.net" title="PHP Hypertext Preprocesor"></a></li>
                <li><a class="apps-apache" href="http://httpd.apache.org" title="APACHE http server project"></a></li>
                <li><a class="apps-smarty" href="http://www.smarty.net" title="Smarty Template System"></a></li>
                <li><a class="apps-vmware" href="http://www.vmware.com"
                       title="VMware Virtualization for Desktop and servers"></a></li>
            </ul>

            <h3 class="phone">24 / 7&nbsp;&nbsp;&nbsp;(+420) - 733 378 437</h3>
        </div>

    </div>
    <div class="clear"></div>
    <div id="footer-bottom-copy">
        <p class="footer-copy-center">Powered by RRsoft Wbengine&nbsp;&copy;&nbsp;2015 All Right Reserved</p>
    </div>
</div>
<script type="text/javascript">
    /**
     * FACEBOOK - declaration
     */
    window.fbAsyncInit = function () {
        FB.init({
            appId: '996786683667586',
            xfbml: true,
            version: 'v2.2'
        });
    };

</script>
{*<script type="text/javascript" src="{$cdn.hostname}/Js/jquery-1.11.1.min.js"></script>*}
{*<script type="text/javascript" src="{$cdn.hostname}/Js/jquery.colorbox-min.js"></script>*}


{if $site_id === 1}
    {*<script type="text/javascript" src="{$cdn.hostname}/Js/jquery.bxslider.min.js"></script>*}
{/if}
<!-- <script type="text/javascript" src="https://apis.google.com/js/platform.js" async defer integrity="sha384-tJ+LRrRjd0nAAQKRZa0hxF25GTp38I7eKzeEn2Q5xSyUAqo4MnmEYk5J87msNds4" crossorigin="anonymous"></script> -->
{*<script type="text/javascript" src="{$cdn.hostname}/Js/jquery-1.11.1.min.js"></script>*}
{*<script type="text/javascript" src="{$cdn.hostname}/Js/jquery.bxslider.min.js"></script>*}
{*<script type="text/javascript" async src="//pagead2.googlesyndication.com/Js/adsbygoogle.js"></script>*}
{*<script type="text/javascript" async src="{$cdn.hostname}/Js/social-buttons.js"></script>*}
<script type="text/javascript">
    /**
     * Googel analytic - merici kod
     */

  (function(i,s,o,g,r,a,m){
	i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-7911733-2', 'auto');
  ga('send', 'pageview');

</script>
{*<script type="text/javascript">*}
    {*/***}
     {** BXSLIDER - declaration and settings*}
     {**/*}
    {*$(document).ready(function () {*}
        {*$('.slider-vmware').show();*}
        {*$('.slider-webhosting').show();*}
        {*$('.slider-network').show();*}
        {*$('.bxslider').bxSlider({*}
            {*auto: true,*}
            {*mode: 'horizontal',*}
            {*speed: 1000,*}
            {*pause: 10000,*}
            {*controls: false*}
        {*});*}
    {*});*}
{*</script>*}

</body>
</html>

