<?php

/**
 * Description of BoxTemplate
 *
 * @author roza
 */

namespace Wbengine\Box;

abstract class xxxBoxTemplate {

    /**
     *
     * @var \Wbengine\Site
     */
    private $site = null;
    public $_box = null;



    /**
     * Return instance of Box object
     * @param Webengine\Box $box
     */
    public function __construct( \Wbengine\Box $box )
    {
	$this->site = $box->getSite();
	$this->_box = $box;
    }


    /**
     * Return inststance of selected renderer
     * @return \Wbengine\Renderer
     */
    public function getRenderer()
    {
	return $this->site->getRenderer();
    }


    public function getBox()
    {
	return $this->_box;
    }

}
