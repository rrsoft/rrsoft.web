<?php

class Class_Site_Box_Central_Store {


    private $_renderer		= null;

    /**
     * Site object as site db row
     * @var <object>
     */
    private $_site	    	= null;
    private $_db		= null;
    private $_urlParts    	= null;
    private $_navigation    	= null;
    private $_lastPart    	= null;
    private $_mediaPath    	= "/Media/Store/Products/";
    private $_boxUrl    	= "produkty";
    private $_boxName    	= "Produkty";
    private $_model		= NULL;


    /**
     * Object constructor nothing todo
     */
    function  __construct(Class_Renderer $renderer)
    {
	if( is_object($renderer) )
	{
	    $this->_site	= $renderer->getCmsObject();
	    $this->_renderer	= $renderer;
	    $this->_db		= $renderer->getDb();

	    $x = $this->getSite()->getUrl();
	    $y = explode('/',$x);

	    $this->_urlParts 	= $y;
	    $this->_navigation  = $this->_site->getNavigation();
	}
	else
	{
	    trigger_error('Parent object can not be empty.');
	}
    }

    private function _getHost()
    {
	return substr($_SERVER['SERVER_PROTOCOL'],0,4).'://'.$_SERVER['HTTP_HOST'];
    }

    private function _getMediaPath($image)
    {
	return $this->_getHost() . $this->_mediaPath . $image;
    }

    private function _getRenderer()
    {
	if ( $this->_renderer instanceof Class_Renderer ) {
	    return $this->_renderer;
	} else {
	    $this->_renderer = $site->getRenderer();
	    return $this->_renderer;
	}

	return null;
    }

    public function getSite()
    {
	if ($this->_site instanceof Class_Site_Abstract ) {
	    return $this->_site;
	} else {
	    trigger_error('Site object is null.');
	}

	return null;
    }

    public function getDb()
    {
	if ($this->_db) {
	    return $this->_db;
	} else {
	    trigger_error('DB object is null.');
	}

	return null;
    }


    /**
     * Create new model object instance.
     */
    private function _setModel()
    {
	require_once 'Class/Site/Box/Central/Store/Model.php';
	$this->_model = new Class_Site_Box_Central_Store_Model();
    }


    /**
     * Get existing or new model object instance.
     * @return Class_Box_Central_Store_Model
     */
    public function getModel()
    {
	if (NULL === $this->_model)
	{
	    $this->_setModel();
	}

	return $this->_model;
    }


    private function _getProduktIdFromUrl($product_tag)
    {
	return preg_replace('/(.*)\-([0-9])/', '$2', $product_tag);
    }

    private function _getProduktIdFromUrlHtml($product_tag)
    {
	return preg_replace('/(.*)\-i([0-9]+)(\.html)/', '$2', $product_tag);
    }

    private function _getProductUrl($id)
    {
	$db = $this->getDb();

	$sql = sprintf("SELECT
			    a.link AS alink,
			    c.link AS clink,
			    p.url AS plink,
			    p.product_id AS id
			FROM store_products p
			LEFT JOIN (store_categories c, store_articles a)
			ON (c.category_id = p.category_id AND a.article_id = p.article_id)
			WHERE product_id = %d
			ORDER BY product_id ASC;"
		    ,$id
		);


	$res = $db->query($sql);

	while ($row = $res->fetch())
	{
	    return $this->_getHost().'/'.$this->_boxUrl.'/'.$row['alink'].'/'.$row['clink'].'/'.$row['plink'].'-'.$row['id'].'/';
	}

    }

    private function _getProductNavigationUrl()
    {
	$db = $this->getDb();
	$link = $this->_urlParts[2];
//	$nav  = self::$_navigation;
	$path = $this->_navigation[sizeof($this->_navigation)-1]['url'];

	if ($this->seo != 'front')
	{
	    $nav[] = array(
		    'url'=>'/',
		    'name'=>'Home',
		);
	    $nav[] = array(
		    'url'=>'/' .$this->_boxUrl.'/',
		    'name'=>$this->_boxName,
		);
	}

	$sql = sprintf("SELECT
			    a.link AS alink,
			    a.article_name,
			    c.link AS clink,
			    c.category_name,
			    p.url,
			    p.product_id AS id,
			    p.product_name AS pname
			FROM store_products p
			LEFT JOIN (store_categories c, store_articles a)
			ON (c.category_id = p.category_id AND a.article_id = p.article_id)
			WHERE product_id = %d
			ORDER BY product_id ASC;"
		    ,$this->_getProduktIdFromUrl($this->_lastPart)
		);

//	die($sql);
	$res = $db->query($sql);

	while ($row = $res->fetch())
	{

	    $_aurl = '/' .$this->_boxUrl.'/'.$row['alink'].'/';
	    $nav[] = array(
		    'url'=>$_aurl,
		    'name'=>$row['article_name'],
		);

	    $_curl = $_aurl.$row['clink'].'/';
	    $nav[] = array(
		    'url'=>$_curl ,
		    'name'=>$row['category_name'],
		);

	    $_url = '/'.$row['plink'].'/';
	    $nav[] = array(
		    'url'=>$this->_getProductUrl($row['id']) ,
		    'name'=>$row['pname'],
		);
	}

	return $nav;
    }

    private function _getCategoryNavigationUrl()
    {
	$db = $this->getDb();
	$link = $this->_urlParts[2];
	$nav  = $this->_navigation;
	$path = $this->_navigation[sizeof($this->_navigation)-1]['url'];

	$sql = sprintf("SELECT s.link AS slink, s.title, a.link AS alink,a.article_name, c.link AS clink, c.category_name
			FROM (store_categories c, store_articles a, cms_sites s)
			WHERE c.article_id = a.article_id
			AND a.site_id = s.site_id
			AND c.link = '%s'
			ORDER BY category_id ASC;"
		    ,$link
		);


	$res = $db->query($sql);

	$row = $res->fetch();

	if( $row )
	{

	    $_url = $path . $row['alink'].'/';
	    $nav[] = array(
		    'url'=>$_url,
		    'name'=>$row['article_name'],
		);

//	    $_url .= $row['clink'].'/';
	    $nav[] = array(
		    'url'=>$_url .$row['clink'].'/',
		    'name'=>$row['category_name'],
		);
	}
	return $nav;
    }

    private function _getArticleNavigationUrl()
    {
	$db = $this->getDb();
	$link = $this->_urlParts[1];

	$parts = $this->_urlParts;
	$nav  = $this->_navigation;
	$path = $this->_navigation[sizeof($this->_navigation)-1]['url'];

	$sql = sprintf("SELECT s.link AS slink, s.title, a.link AS alink, a.article_name
			FROM (store_articles a, cms_sites s)
			WHERE a.site_id = s.site_id
			AND a.link = '%s'
			ORDER BY article_id ASC;"
		    ,$link
		);

	$res = $db->query($sql);

	$row = $res->fetch();

	if ($row)
	{
//die($sql);
	    $nav[] = array(
		    'url'=>$path.$row['alink'].'/',
		    'name'=>$row['article_name'],
		);

	}
	return $nav;
    }

    public function getMainBox()
    {
	$db	= $this->getDb();
	$site	= $this->getSite();
	$r	= $this->_getRenderer();
	$t	= $this->_getRenderer();

	$add	= (isset($_POST['b_store_add'])) ? true : false;

	if(!$t) {
	    trigger_error('Error: Renderer is null');
	}

	// Add item to store
	if ($add){
	    $result = $this->_addItemToBasket(request_var('product_id',0));
	    if ($result === TRUE){
		$site->vars['title'] = $site->getLocale()->STORE_BASKET_TITLE;
		$site->vars['msg'] = sprintf($site->getLocale()->STORE_BASKET_ADD_SUCCESS,$t->getHost().'/basket/',$t->getHost().$_SERVER['REQUEST_URI']);
		header('Refresh: 4; URL='.$t->getHost().$_SERVER['REQUEST_URI']);

		return $t->render('message', $site->vars);
	    }else{
		return $result;
	    }
	}

	$this->_lastPart = $this->_urlParts[(sizeof($this->_urlParts)-1)];

//	if (preg_match('/(.html)/', self::$_lastPart) && sizeof(self::$_urlParts) > 3) {
	if (sizeof($this->_urlParts) > 3) {
	    $r->setVar('pnav', $this->_getProductNavigationUrl());
	    $product_id = $this->_getProduktIdFromUrl($this->_lastPart);
	    return $this->getProduktDetailBox($product_id);
	}

	if ( sizeof($this->_urlParts) == 1 ) {
//	    return $this->getArticlesBox();
	    return $this->getProduktListBox();
	} elseif ( sizeof($this->_urlParts) == 2 ) {
	    $r->setVar('nav', $this->_getArticleNavigationUrl());
	    return $this->getCategoriesBox($this->getArticleIdFromUrl());
	} elseif ( sizeof($this->_urlParts) == 3 ) {
//	    $r->setVar('nav', $this->_getCategoryNavigationUrl());
	    $r->setVar('nav', $this->_getProductNavigationUrl());
	    return $this->getProduktsBox($this->getCategoryIdFromUrl());
	}
    }

    public function getArticleIdFromUrl()
    {
	$db = $this->getDb();
	$link = $this->_urlParts[1];

	$sql = sprintf("SELECT article_id FROM store_articles
			WHERE link = '%s'
			ORDER BY article_id ASC;"
		    ,$link
		);

	$res = $db->query($sql);

	$row = $res->fetch();

	if($row) {
	    return $row['article_id'];
	}

	return null;
    }

    public function getCategoryIdFromUrl()
    {
	$db = $this->getDb();
	$link = $this->_urlParts[2];

	$sql = sprintf("SELECT category_id FROM store_categories
			WHERE link = '%s'
			ORDER BY article_id ASC;"
		    ,$link
		);

	$res = $db->query($sql);

	$row = $res->fetch();

	if($row) {
	    return $row['category_id'];
	}

	return null;

    }

    public function getArticlesBox()
    {
	$db	= $this->getDb();
	$r	= $this->_getRenderer();

	$sql = sprintf("SELECT * FROM store_articles
			ORDER BY article_id ASC;"
	);

	$result = $db->sql_query($sql);

	while($row = $db->sql_fetchrow($result))
	{
	    $article[$row['article_id']]['name'] = $row['article_name'];
	    $article[$row['article_id']]['url'] = $r->createUrl($row['article_name']);
	}

	if (sizeof($article))
	{
	    return $r->render('store_articles', $article);
	} else {
	    return null;
	}
    }

    public function getCategoriesBox($article_id = null)
    {
	$db	= $this->getDb();
	$site	= $this->getSite();
	$r	= $this->_getRenderer();
	$t	= $this->_getRenderer();

	if(!$t) {
	    trigger_error('Error: Renderer is null');
	}

	$sql = sprintf("SELECT * FROM store_categories
			WHERE article_id = %d
			ORDER BY category_name ASC;"
		    ,$article_id
		);

	$res = $db->query($sql);

	while ($row = $res->fetch())
        {
	    $categories[$row['category_id']]['name'] = $row['category_name'];
	    $categories[$row['category_id']]['url'] = $r->createUrl($row['link']);
	}

	if (sizeof($categories)) {
	    return $t->render('store_categories', $categories);
	} else {
	    return null;
	}
    }

    public function getProduktsBox($category_id)
    {
        if(! $t = $this->_getRenderer()) {
            trigger_error('Error: Renderer is null');
	}

	$db	= $this->getDb();
	$site	= $this->getSite();
	$r	= $this->_getRenderer();
	$parts	= $site->getUrlParts();

        $sql = sprintf("SELECT * FROM store_products p
			WHERE p.category_id = %s
			AND p.article_id =
			(SELECT a.article_id FROM store_articles a WHERE a.link = '%s')
			AND p.category_id =
			(SELECT c.category_id FROM store_categories c WHERE c.link = '%s')
			ORDER BY product_name ASC;"

		,(int)$category_id
		,$parts[1]
		,$parts[2]
	    );
//die($sql);
	$res = $db->query($sql);

	while ($row = $res->fetch())
	{
	    // add product URL
	    $row['url'] = (empty($row['url']))
		? $r->createUrl($row['product_name'].'-i' . $row['product_id'], true)
		: $row['url'];

	    $row['image'] = $this->_getProductScreenshot($row['product_id']);
	    $row['full_url'] = $this->_getProductUrl($row['product_id']);

	    // add product price with tax
	    $dph = ($row['dph']/100)+1;
	    $row['price_dph'] = ($row['dph'])
		? ceil((int)$row['price']*$dph)
		: ceil($row['price']);

	    // add product save price
	    $save = ($row['save']/100)*$row['price'];
	    $row['price_save'] = ($save)
		? ceil( ($row['price']-$save) * $dph)
		: ceil($row['price']);

	    $produkts[] = $row;
	}

	if (sizeof($produkts)) {
	    return $t->render('store_products', $produkts);
	} else {
	    return null;
	}
    }

    public function getProduktDetailBox($product_id)
    {
        if(! $t = $this->_getRenderer()) {
            trigger_error('Error: Renderer is null');
	}

	$db	= $this->getDb();
	$site	= $this->getSite();
	$r	= $this->_getRenderer();
        $parts  = $site->getUrlParts();

        $sql = sprintf("SELECT * FROM store_products p
			WHERE p.article_id =
			    (SELECT a.article_id FROM store_articles a WHERE a.link = '%s')
			AND p.category_id =
			    (SELECT c.category_id FROM store_categories c WHERE c.link = '%s')
			AND p.product_id = %d
			ORDER BY product_name ASC;"

		,$parts[1]
		,$parts[2]
		,(int)$product_id
	    );

	$res = $db->query($sql);

	$row = $res->fetch();

	if ($row) {
	    $row['image'] = $this->_getProductScreenshot($row['product_id']);
//	    $row['pnav'] = $this->_getProductNavigationUrl();
	    $r->setHtmlIdentity($row['product_name'],$row['product_name'],'zboží, produkty a tak dále');
	    return $t->render('store_product_detail', $row);
	} else {
	    return null;
	}
    }

    public function getProduktListBox($items = 20, $page = 0)
    {
        if(! $t = $this->_getRenderer()) {
            trigger_error('Error: Renderer is null');
	}

	$db	= $this->getDb();
	$site	= $this->getSite();
	$r	= $this->_getRenderer();
	$i	= 0;

        $sql = sprintf("SELECT * FROM %s p
			ORDER BY product_name ASC LIMIT %s;"
		,S_TABLE_STORE_PRODUCTS
		,$page.','.$items
	    );

	$res = $db->query($sql);

	while ($row = $res->fetch())
	{
	    // add product URL
	    $row['url'] = (empty($row['url']))
		? $r->createUrl($row['product_name'].'-i' . $row['product_id'], true)
		: $row['url'];

	    // add product price with tax
	    $dph = ($row['dph']/100)+1;
	    $row['price_dph'] = ($row['dph'])
		? ceil((int)$row['price']*$dph)
		: ceil($row['price']);

	    // add product save price
	    $save = ($row['save']/100)*$row['price'];
	    $row['price_save'] = ($save)
		? ceil( ($row['price']-$save) * $dph)
		: ceil($row['price']);

	    $row['full_url'] = $this->_getProductUrl($row['product_id']);
	    $row['image'] = $this->_getProductScreenshot($row['product_id']);
	    $row['class'] = ($i) ? 'class' : '';
	    $row['perex'] = createProductPerex($row['perex'], 215);
	    $products[] = $row;
	    $i++;
	}

	if (sizeof($products)) {
	    return $t->render('store_product_list', $products);
	} else {
	    return null;
	}

    }

    /**
     * This method returns media path for given
     * product id.
     * If there more images exist, you can specify
     * image over index.
     *
     * @param integer $id as product id
     * @param integer $index as image index
     * @return string
     */
    private function _getProductScreenshot($id, $index = 0) {
	$file = $id."_img_".$index.".jpg";
	return $this->_getMediaPath($file);
    }

    private function _getBasketProducts()
    {
        $rows = $this->getModel()->getProductsFromBasket();

	if (!is_array($rows) && sizeof($rows)== 0)
	    return NULL;

	foreach ($rows as $row)
        {

	    // add product URL
	    $row['url'] = (empty($row['url']))
		? $t->createUrl($row['product_name'].'-i' . $row['product_id'], true)
		: $row['url'];

	    // add product price with tax
	    $dph = ($row['dph']/100)+1;

	    $row['price_dph'] = ($row['dph'])
		? ceil((int)$row['price']*$dph)
		: ceil($row['price']);

	    // add product save price
	    $save = ($row['save']/100)*$row['price'];

	    $row['price_save'] = number_format(($save)
		? ceil( ($row['price']-$save) * $dph)
		: ceil($row['price']), 2, '.', ' ');

	    $row['full_url'] = $this->_getProductUrl($row['product_id']);
	    $row['image'] = $this->_getProductScreenshot($row['product_id']);
	    $row['class'] = ($i) ? 'class' : '';
	    $row['perex'] = createProductPerex($row['perex'], 215);

	    $y = ($row['price']*$row['count']);
	    $row['price_count'] = number_format($y, 2, '.', ' ');
	    $x = $x + $y;

	    $products[] = $row;
	    $i++;
	}

	return $products;
    }

    /**
     * This method try to add product to basket.
     * Returns true on success OR mixed string as
     * error message.
     *
     * @access private
     * @return mixed
     */
    private function _addItemToBasket()
    {
	$db	= $this->getDb();
	$site	= $this->getSite();

	if(! $t = $this->_getRenderer())
	    trigger_error('Error: Renderer is null');

	$count	= (int)request_var('t_ks', 0);

	if ($count == 0){
	    $err[] = $site->getLocale()->STORE_BASKET_ERROR_COUNT;
	}

	$sql = sprintf("INSERT INTO %s
			    (product_id, user_id, session_id, count, created_time, user_ip, status)
			VALUES
			    ('%s','%s','%s','%s','%s','%s','%s');"
		,S_TABLE_STORE_BASKET
		,request_var('product_id', 0)
		,$site->getSession()->getValue('user_id')
		,session_id()
		,request_var('t_ks', 0)
		,time()
		,Class_Enviroment::getUserIp()
		,STORE_STATUS_ADDED
	);

	if (sizeof($err)){
	    $site->vars['title'] = $site->getLocale()->STORE_BASKET_TITLE;
	    $site->vars['msg'] = (sizeof($err)) ? implode('<br />', $err) : '';

	    header('Refresh: 4; URL='.$t->getHost().$_SERVER['REQUEST_URI']);
	    return $t->render('error',$site->vars);
	}else{
	    return ($res = $db->query($sql)->rowCount()) ? TRUE : FALSE;
	}

    }

    public function getBasketBox()
    {
	$site	= $this->getSite();

	if(! $t = $this->_getRenderer())
	    trigger_error('Error: Renderer is null');

	$site->vars['products'] = $this->_getBasketProducts();
	$site->vars['sum'] = number_format($x+$save, 2, '.', ' ');

        if ($_REQUEST['b_store_pay'])
        {
            return $delivery_types = $t->render('store_basket_delivery_types',$this->getModel()->getDeliveryTypes());
        }

        $site->vars['delivery_types'] = $delivery_types;

        return $t->render('store_basket_list', $site->vars);
    }
}