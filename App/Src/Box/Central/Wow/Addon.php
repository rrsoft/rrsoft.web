<?php

/**
 * $Id: Doors.php 85 2010-07-07 17:42:43Z bajt $
 * ----------------------------------------------
 * Class Box Central WoW sub module class.
 *
 * @package RRsoft-CMS
 * @version $Rev: 30 $
 * @copyright (c) 2012 RRsoft www.rrsoft.cz
 * @license GNU Public License
 *
 * Minimum Requirement: PHP 5.3.x
 */

namespace App\Box\Central\Wow;

use Wbengine\Box\BoxTemplate;
use Wbengine\Config;
use Wbengine\Utils;

/**
 * @property mixed name
 * @property mixed id
 * @property mixed tag
 */
class Addon extends BoxTemplate
{

    /**
     * Self addon data
     * @var \App\Box\Central\WowAddon
     */
    private $_addon = NULL;

    /**
     * Addons collection
     * @var array
     */
    private $_addons = NULL;

    /**
     * Stored Parent object
     * @var null
     */
    private $_parent = NULL;

    /**
     * Addon detail url
     * @var string
     */
    private $detail_url = NULL;



    /**
     * Global getter addon object properties
     * @param $var
     * @throws WowException
     */
    function __get($var)
    {
        if ($this->_addon) {
            return $this->_addon->$var;
        } else {
            return null;
//            throw new WowException(__METHOD__
//                . ': Addon data is empty or not loaded.');
        }
    }


    /**
     * In constructor we just prepare all for addon object creation
     * @param BoxTemplate $parent
     * @param null $addonTag
     */
    function __construct(BoxTemplate $parent, $addonTag = NULL)
    {
        parent::__construct($parent->getBox());
        $this->_parent = $parent;

        if (NULL != $addonTag) {
            $this->getAddon($addonTag);
        }
    }


    /**
     * Return parent object
     * @return object|BoxTemplate
     */
    public function getParent()
    {
        return $this->_parent;
    }


    /**
     * Return created associated array with all images
     * for renderer
     * @param $id
     * @return mixed
     */
    public function getAddonImages($id)
    {
        // check for addon reference
        if ($this->ref_id > 0)
            $id = $this->ref_id;

        // test zda existuji vice screenshotu
        for ($i = 0; $i < 4; $i++) {

            // sestavi se thumb nazev souboru
            $file = Config::getCdnPath()."/Media/Addons/Thumb/" . $id . "_img_" . $i . ".jpg";
            // sestavi se nazev souboru normalni velikosti na preview
            $link = Config::getCdnPath()."/Media/Addons/Full/" . $id . "_img_" . $i . ".jpg";
            // jestlize soubor existuje sestavi se img tag
            if (is_readable($_SERVER["DOCUMENT_ROOT"] . $link)) {

                $data[$i]["thumb"] = $file;
                $data[$i]["link"] = $link;
                $data[$i]["detail_url"] = $this->get;
                $data[$i]["alt"] = $this->_addon->name;
            }
        }
        return $data;
    }


    /**
     * This method just a prepare addon's detail link
     */
    public function _setDetailUrl()
    {
        $this->_addon->tag = preg_replace("/^(.*)-[0-9]{3}/", "$1", $this->tag);
        $this->_addon->detail_url = $this->getSite()->getUrl() . $this->tag . "/";
    }


    /**
     * This method just a prepare download link...
     */
    private function _setDownloadLink()
    {
        $this->_addon->download_link =
            $this->getSite()->getHomeUrl()
            . "/_addons/"
            . strtolower(preg_replace('/\s/', "_", $this->name))
            . "_"
            . $this->version
            . "_"
            . $this->patch
            . ".zip";
    }

    /**
     * Prepare all addon's data and return it as object
     * @param $addonTag
     * @return $this
     * @throws \Wbengine\Box\Exception\BoxException
     */
    public function getAddon($addonTag)
    {
        $this->_addon = $this->getModel(__CLASS__)->getAddonByTag($addonTag);
        if($this->_addon) {
            $this->_setDetailUrl();
            $this->_setDownloadLink();

            return $this;
        }else{
            throw new WowException(__METHOD__
                . ': Addon not found.', 404);

            return null;
        }
    }


    /**
     * @param $patch
     * @return null
     * @throws \Wbengine\Box\Exception\BoxException
     */
    public function getAddonsByPatch($patch)
    {
        $addons = $this->getModel(__CLASS__)->getAddonsByPatch($patch);

        if (sizeof($addons) === 0) {
            throw new WowException(__METHOD__
                . ': Addon not found.', 404);
        }

        foreach ($addons as $addon) {
            $this->_addons[] = new Addon($this->getParent(), $addon->tag);
        }

        return $this->_addons;
    }


    /**
     * Return Addons list as array By Patch
     * @return array
     * @throws \Wbengine\Box\Exception\BoxException
     */
    public function getPatches()
    {
        return $this->getModel(__CLASS__)->getAdonsTypesByPatch();
    }

    /**
     * Return Addons list as array By Patch
     * @param mixed $keyword
     * @return array
     * @throws \Wbengine\Box\Exception\BoxException
     */
    public function getAddonsByPatchWithKeyword($keyword = null)
    {
//        header("HTTP/1.1 404 Not Found");
        return $this->getModel(__CLASS__)->getAdonsByPatchWithKeyword($keyword);
    }


    /**
     * Return \App\Box\Central\WowAddon as raw array from DB
     * @return array
     */
    public function toArray()
    {
        return $this->_addon;
    }

}
