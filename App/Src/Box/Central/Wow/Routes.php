<?php

return array(
    array(
        '/addons/$',
        array(
            'controller' => "wow",
            'action' => 'getPatchesBox',
            'params' => array(
                'action' => 1,
            )
        )),

    array(
        '/addons/patch-([0-9]{3})/$',
        array(
            'controller' => "wow",
            'action' => 'getAddonsBox',
            'params' => array(
                'patch' => 2,
            )
        )),

    array(
        '/addons/patch-([0-9a-z\.\-]+)/([0-9a-z\-]+)/$',
        array(
            'controller' => "wow",
            'action' => 'getAddonDetailBox',
            'params' => array(
                'patch' => 2,
                'tag' => 3,
            )
        )),

    array(
        '/addons/patch-([0-9a-z\.\-]+)/[0-9a-z\-]+[-a]([0-9]+)/$',
        array(
            'controller' => "wow",
            'action' => 'getAddonDetailBox',
            'params' => array(
                'id' => 3,
            )
        )),

);

