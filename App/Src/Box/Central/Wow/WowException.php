<?php

 /**
 * @see Class_Exception
 */

namespace App\Box\Central\Wow;

use Wbengine\Box\Exception\BoxException;

class WowException extends BoxException
{
}
