<?php

/**
 * $Id: Model.php 85 2010-07-07 17:42:43Z bajt $
 * ----------------------------------------------
 * Site's object Class_Site data Model.
 *
 * @package RRsoft-CMS
 * @version $Rev: 30 $
 * @copyright (c) 2009-2010 RRsoft www.rrsoft.cz
 * @license GNU Public License
 *
 * Minimum Requirement: PHP 5.1.x
 */

namespace App\Box\Central\Wow\Addon;

use Wbengine\Model\ModelAbstract;

class Model extends ModelAbstract {


    /**
     * Return current site title by given
     * url.
     *
     * @param string $part
     * @return string
     */
    public function getAddonsByPatch( $patch = NULL )
    {
	$where = array(
	    $patch);

	$sql = sprintf("SELECT *, DATE_FORMAT(a.date, '%s') AS datum FROM %s a
                        WHERE a.patch = ?
                        ORDER BY a.name;"
		, "%d.%m.%Y"
		, S_TABLE_WOW_ADDONS
		, $patch
	);
//        var_dump($sql);
	$res = $this->getDbAdapter()->query($sql, array(
	    $patch));

	return $res;
//	$res = $this->getDb()->query($sql, $where);
//	foreach ( $res as $r ) {
//	    var_dump($resd);
//	}
//	var_dump($res);
//	return $res->fetchAll();
//	return $this->getDb()->query($sql);
//	return $this->getDb()->fetchAll($sql);
    }


    public function getAdonsTypesByPatch()
    {
	$sql = sprintf("SELECT p.*, DATE_FORMAT(p.released,'%s') AS released FROM %s a
                        INNER JOIN %s p ON (p.tag = a.patch_label)
                        GROUP BY a.patch_label ORDER BY patch_label DESC;"
		, "%d.%m.%Y"
        , S_TABLE_WOW_ADDONS
		, S_TABLE_WOW_PATCHES
	);

        $rec = ($this->getDbAdapter()->query($sql,array()));
//        var_dump($rec->current());
        return $rec;
//	return $this->getDbAdapter()->fetchAll($sql);
    }

    public function getAdonsByPatchWithKeyword($keyword=null)
    {
        $sql = sprintf("SELECT p.*, DATE_FORMAT(p.released,'%s') AS released, a.name,a.tag AS addon_tag, a.version, a.patch FROM %s a
                        INNER JOIN %s p ON (p.tag = a.patch_label)
                        WHERE a.tag LIKE '%s'
                        GROUP BY a.patch_label ORDER BY patch_label DESC;"
            , "%d.%m.%Y"
            , S_TABLE_WOW_ADDONS
            , S_TABLE_WOW_PATCHES
            , trim($keyword)."%"
        );
//        var_dump ($sql);
        $rec = ($this->getDbAdapter()->query($sql,array()));
//        var_dump($rec->current());
//        var_dump($rec);
        return $rec;
//	return $this->getDbAdapter()->fetchAll($sql);
    }


    public function getAddonById( $addonId )
    {
	$sql = sprintf("SELECT *, if(a.ref_id > 0,
                        (SELECT description FROM %s b
                        WHERE a.ref_id = b.id), a.description) AS description,
                        (SELECT COUNT(id) FROM %s WHERE idaddon = a.id) AS comments,
                        DATE_FORMAT(a.date, '%s') AS datum FROM %s a
                        WHERE a.id = ?
                        ORDER BY a.name;"
		, S_TABLE_WOW_ADDONS
		, S_TABLE_COMMENTS
		, "%d.%m.%Y"
		, S_TABLE_WOW_ADDONS
		, $addonId
	);

	$res = $this->getDbAdapter()->query($sql, array(
	    $addonId));
        return $res->current();

//	return $this->getDb()->fetchRow($sql);
    }



    public function getAddonByTag( $addonTag )
    {
//        die($addonTag);
	$sql = sprintf("SELECT *, if(a.ref_id > 0,
                        (SELECT description FROM %s b
                        WHERE a.ref_id = b.id), a.description) AS description,
                        (SELECT COUNT(id) FROM %s WHERE idaddon = a.id) AS comments,
                        DATE_FORMAT(a.date, '%s') AS datum FROM %s a
                        WHERE a.tag = ?;"
		, S_TABLE_WOW_ADDONS
		, S_TABLE_COMMENTS
		, "%d.%m.%Y"
		, S_TABLE_WOW_ADDONS
		, $addonTag
	);
//        var_dump($sql);
	$res = $this->getDbAdapter()->query($sql, array(
	    $addonTag));
        return $res->current();

//	return $this->getDb()->fetchRow($sql);
    }

}
