<?php

/**
 * $Id: Doors.php 85 2010-07-07 17:42:43Z bajt $
 * ----------------------------------------------
 * Class Central Doors module class.
 *
 * @package RRsoft-CMS
 * @version $Rev: 30 $
 * @copyright (c) 2009-2010 RRsoft www.rrsoft.cz
 * @license GNU Public License
 *
 * Minimum Requirement: PHP 5.1.x
 */

namespace App\Box\Central;

use App\Box\Central\Wow\Addon;
use App\Box\Central\Wow\WowException;
use Wbengine\Box\BoxTemplate;
use Wbengine\Renderer;
use Wbengine\Router;
use Wbengine\Site;

class Wow extends BoxTemplate
{

//    private $_addonClass = NULL;
//    private $_methodName = NULL;


    /**
     * URL box params identifier.
     * @var string
     */
    private $_paramIdentifier = "-";


    /**
     * Pattern for the argument parser.
     * @var string
     */
    private $_pattern = '/^(.*)##(.*)(.html|\/)$/';

    /**
     * Detail addon class object
     * @var Wow\Addon
     */
    private $_addonClass = null;

//    private $_pattern   = "/^(.*)-(.*)$/";


    /**
     * MAIN ROUTE BOX!
     * @throws Router\RouterException
     * @internal param Site $site
     * @return string
     */
    public function getWowBox()
    {
        $this->setRoutes(include dirname(__FILE__) . "/Wow/Routes.php");
        $router = New Router($this);

        $matchRoute = $router->match();
//var_dump($matchRoute);
        if (md5($this->getSite()->getUrl()) === md5($this->getBoxUrl())) {
            return $this->getIndexBox();
        }


        if ($matchRoute instanceof Router\Route) {
            $methodName = $matchRoute->getAction();
//            var_dump($methodName);
//            var_dump($matchRoute->getController());
//            var_dump($matchRoute->getAction());
//            return $this->getIndexBox();
//            var_dump($matchRoute->getParams());
            return $this->$methodName($matchRoute->getParams());// $methodName;
        }

//        die();
        $this->createException("Page Not found", HTML_ERROR_404);
        return $this->getRenderer()->getErrorBox($this->getException());


//        die(Error404);


    }

    public function getAddonsBox($params)
    {
//        var_dump($params[patch]);
        return $this->_getAddonsByPatch($params['patch']);
    }

    /**
     * This method try to find box params in url and
     * store it for latest linked box use.
     *
     * As next, create and return box method name from
     * last part url or create and return box method name
     * from penultimate url part instead, when any box
     * parameters has matched in last url part by the
     * defined identifier and mask pattern.
     *
     * @return string
     */
    public function getMethodNameFromBoxUrl()
    {
        $_urlParts = $this->getUrlParts();
        $this->_boxParams = $this->getBoxParamsFromUrl();
//        var_dump($this->_boxParams);
        if ($this->_boxParams === null) {
            $_part = $_urlParts[sizeof($_urlParts) - 1];
        } else {
//            $this->setBoxParams($this->getBoxParamsFromUrl());
            $_part = $_urlParts[sizeof($_urlParts) - 2];
        }

//        var_dump($_part);
//        var_dump($this->getBox()->_createMethodName($_part));
        return $this->getBox()->_createMethodName($_part);
    }


    /**
     * Return box arguments from posted URL
     * @return string
     */
    public function getBoxParamsFromUrl()
    {

//        $_url = ($_url = strstr($this->getSite()->getLink(), ".html", true)) ? $_url : $this->getSite()->getLink();
//        var_dump($_url);
//        $_url = trim($_url, "/");
//        $_mask = str_replace("#", $this->_paramIdentifier, $this->_pattern);
//        var_dump(strstr($this->getSite()->getLink(), $this->_paramIdentifier));
//                var_dump(strstr($this->getSite()->getLink(), $this->_paramIdentifier));
//var_dump(preg_replace($this->_pattern, "$2", $this->getSite()->getLink()));
        if (!empty($this->_paramIdentifier) && strstr($this->getSite()->getLink(), $this->_paramIdentifier) === false) {
            return null;
        }
//            return false;
//die;
//        var_dump($this->_createPattern());
//        var_dump($this->getSite()->getLink());
//        var_dump(preg_replace($this->_createPattern(), "$2", $this->getSite()->getLink()));
        return preg_replace($this->_createPattern(), "$2", $this->getSite()->getLink());
    }


    private function _getAddonsByPatch($patch = null)
    {
//        var_dump($this->getAddonClass()->getAddonsByPatch($patch));
//        foreach ($this->getAddonClass()->getAddonsByPatch($patch) as $value) {
//            var_dump($value->name);
//        }
//        $this->getAddonClass()->getAddon(131);
//        $xx=$this->getAddonClass()->getAddonImages(131);
//        var_dump($xx["thumb"]);
//        $xx = $this->getAddonClass()->getAddonsByPatch($patch);
//        $yy = $xx[0];
//        var_dump($this->getAddonClass()->getAddonsByPatch($patch));
//        var_dump($patch);
        $_data = $this->getAddonClass()->getAddonsByPatch($patch);
//        var_dump(implode(".",preg_split('//', $patch, 0, PREG_SPLIT_NO_EMPTY)));

//	foreach ( $_data as $addon ) {
//	    var_dump($addon->name);
//	}
//	var_dump($_data);
        $_patch = implode(".", str_split($patch));
        $this->getSite()->setHtmlTitle("Výběr addonů do hry World Of Warcraft pro patch " . $_patch);
        $this->getSite()->setHtmlDescription("Seznam nejoblíbenějších addonů do populární hry World Of Warcraft pro verzi " . $_patch . "");
        $this->getSite()->setHtmlKeywords("patch " . $_patch . ",addony,addons,wow,seznam");

        if (!empty($_data)) {
            $this->getRenderer()->assign('patch_label', implode(".", preg_split('//', $patch, 0, PREG_SPLIT_NO_EMPTY)));
            return $this->getSite()->getRenderer()->render('Central/Wow/addons_list', $_data);
        } else {
            $this->createException("Page Not found", HTML_ERROR_404);
            return $this->getRenderer()->getErrorBox($this->getException());
        }
    }


    /**
     * Store Box arguments for latest use
     * @param mixed $params
     */
    public function setBoxParams($params)
    {
        $this->_boxParams = $params;
    }

    public function getBoxRootContent()
    {
        return $this->getSite()->getRenderer()->render('Central/Wow/index');
    }

    /**
     * Return stored Box arguments.
     * @return
     * @internal param mixed $params
     */
    public function getBoxParams()
    {
        return $this->_boxParams;
    }


    private function _process($methodName)
    {
        if (method_exists($this, $methodName)) {
            return $this->$methodName($this->getSite());
        }

        $this->createException("Page Not found", HTML_ERROR_404);
        return $this->getRenderer()->getErrorBox($this->getException());
    }


    /**
     * Return all box URL parts as array.
     * @return array
     */
    public function getUrlParts()
    {
        $x = $this->getSite()->getLink();

        $y = explode('/', trim($x, '/'));

        return $y;
    }


    /**
     * Create patern for url param parser.
     * @return string
     */
    private function _createPattern()
    {
        return str_replace("##", $this->_paramIdentifier, $this->_pattern);
    }


    /**
     * @return Wow\Addon
     */
    public function getAddonClass()
    {
        if (NULL === $this->_addonClass) {
            $this->_addonClass = new Addon($this);
        }

        return $this->_addonClass;
    }


    public function getIndexBox()
    {
        $this->getRenderer()->assign("patches_list", $this->getAddonClass()->getPatches());
        return $this->getSite()->getRenderer()->render('Central/Wow/index');
    }


    public function getPatchesBox()
    {
        try {
            $params = $this->getSite()->getUrlParams();
            if (is_array($params)) {
                $search = $params[search];
                $keyword = $params[keyword];
                $patch = $params[patch];
            }

            if (!empty($patch)) {
                $_keyword = $this->getAddonClass()->getAddon($keyword .= "-" . $patch)->tag;
            } else {
                $_keyword = $keyword;
            }

            if (!is_null($params) && $search == 1 && !is_null($keyword)) {
                $this->getSite()->setHtmlTitle("Výsledek hledání addonu podle klíče " . trim($_keyword));
                $this->getSite()->setHtmlDescription("Výsledek hledání addonu " . trim($_keyword) . " ve vydaných aktualizacích hry Worl Of Warctaft Patches");
                $this->getSite()->setHtmlKeywords("Seznam addonů,aktualizace,addony,addons,patch,wow");

                $this->getSite()->getRenderer()->assign("keyword", $_keyword);
                return $this->getSite()->getRenderer()->render('Central/Wow/search_addons_patches_list', $this->getAddonClass()->getAddonsByPatchWithKeyword($keyword));
            } else {
                $this->getSite()->setHtmlTitle("World Of Warcraft - seznam aktualizací - Patches");
                $this->getSite()->setHtmlDescription("Výběr nejoblíbenějších addonů podle vydaných aktualizací - Worl Of Warctaft Patches");
                $this->getSite()->setHtmlKeywords("Seznam addonů,aktualizace,addony,addons,patch,wow");

                return $this->getSite()->getRenderer()->render('Central/Wow/patches_list', $this->getAddonClass()->getPatches());

            }
        } catch (WowException $e) {
            $this->getSite()->setHtmlTitle("World Of Warcraft - seznam aktualizací - Vyhledávání " . $keyword);
            $this->getSite()->setHtmlDescription("Výběr nejoblíbenějších addonů podle vydaných aktualizací - Worl Of Warctaft Vyhledávání " . $keyword);
            $this->getSite()->setHtmlKeywords("Seznam addonů,aktualizace,addony,addons,patch,wow");
            return $this->getSite()->getRenderer()->render('Central/Wow/patches_list', $this->getAddonClass()->getPatches());
        }
    }


    /**
     * WOW Addon Class
     *
     * Return WoW Addon Box info page
     * @param $params
     * @return string
     * @throws \Wbengine\Exception\RuntimeException
     */
    public function getAddonDetailBox($params)
    {
        $_tag = $params[tag] . "-" . $params[patch];

        try {
            $data = $this->getAddonClass()->getAddon($_tag);
            $this->getSite()->setHtmlTitle($data->name . " - v" . $data->version . " pro Patch " . $data->patch_label);
            $this->getSite()->setHtmlDescription("Stáhnout addon " . $data->name . " - v" . $data->version . " pro Patch " . $data->patch_label . " - Download");
            $this->getSite()->setHtmlKeywords(strtolower($data->name) . ",addon,wow,stáhnout,download");

            $this->getRenderer()->assign("description", $this->getRenderer()->getFormater()->process($data->description));
            $this->getRenderer()->assign("url", $this->getSite()->getUrl());
            return $this->getSite()->getRenderer()->render('Central/Wow/addon_detail', $data);
        } catch (WowException $e) {
            $this->getRenderer()->assign("error", $e->getMessage());
            $this->createException("Page Not found", HTML_ERROR_404);
            return $this->getRenderer()->getErrorBox($this->getException());
//            return $this->getSite()->getRenderer()->render('Central/Wow/addon_detail');

        }

    }

}
