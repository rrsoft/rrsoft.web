<?php

/**
 * $Id: Story.php 85 2010-07-07 17:42:43Z bajt $
 * ----------------------------------------------
 * Class Central Story module class.
 *
 * @package RRsoft-CMS
 * @version $Rev: 30 $
 * @copyright (c) 2009-2010 RRsoft www.rrsoft.cz
 * @license GNU Public License
 *
 * Minimum Requirement: PHP 5.1.x
 */
//require_once 'Class/Site/Box/Abstract.php';

namespace App\Box\Central;

class Article extends \Wbengine\Box\BoxTemplate
{


    /**
     * Return story content from table article
     * @return string
     */
    public function getArticleBox()
    {
        $row = $this->getModel(__CLASS__)->getArticleRow();
        return $this->getRenderer()->render('story_box', $row->introtext);
    }


    public function getContactsBox($site)
    {
        if (!$t = $this->getRenderer())
            trigger_error('Error: Renderer is null');

        return $t->render('contacts');
    }


    public function getFrontSplashBox($site)
    {
        if (!$t = $this->getRenderer())
            dtrigger_error('Error: Renderer is null');

        return $t->render('Central/front-splash');
    }


    public function getFrontIntroBox($site)
    {
        if (!$t = $this->getRenderer())
            dtrigger_error('Error: Renderer is null');

        return $t->render('Central/front-intro');
    }


    public function getSeoFrontBox($site)
    {
        if (!$t = $this->getRenderer())
            dtrigger_error('Error: Renderer is null');

        return $t->render('seo_front');
    }


    public function getTrippleboxBox($site)
    {
        if (!$t = $this->getRenderer())
            dtrigger_error('Error: Renderer is null');

        return $t->render('Central/tripple_box');
    }


    public function getMininavigatorBox($site)
    {
        if (!$t = $this->getRenderer())
            dtrigger_error('Error: Renderer is null');

        return $t->render('Central/mini_navigator');
    }


    public function getVmwareSplashBox($site)
    {
        if (!$t = $this->getRenderer())
            dtrigger_error('Error: Renderer is null');

        return $t->render('Central/vmware_splash');
    }


    public function getSliderBox($site)
    {
        if (!$t = $this->getRenderer())
            dtrigger_error('Error: Renderer is null');

        return $t->render(sprintf('Central/slide%d', rand(1,2)));
    }

    public function getWebhostingSplashBox($site)
    {
        if (!$t = $this->getRenderer())
            dtrigger_error('Error: Renderer is null');

        return $t->render('Central/webhosting_splash');
    }

    public function getRegistraceDomenyBox($site)
    {
        if (!$t = $this->getRenderer())
            dtrigger_error('Error: Renderer is null');

        return $t->render('Central/registrace_domeny');
    }

    public function getHostingsBox($site)
    {
        if (!$t = $this->getRenderer())
            dtrigger_error('Error: Renderer is null');

        return $t->render('Central/hostings');
    }

    public function getSocialMarketingBox($site)
    {
        if (!$t = $this->getRenderer())
            dtrigger_error('Error: Renderer is null');

        return $t->render('Central/social_marketing');
    }

    public function getModernBackupBox($site)
    {
        if (!$t = $this->getRenderer())
            dtrigger_error('Error: Renderer is null');

        return $t->render('Central/moderni_zalohovani_dat');
    }

    public function getRelatedStoriesFrontBox($site)
    {
        if (!$t = $this->getRenderer())
            dtrigger_error('Error: Renderer is null');

        return $t->render('Central/related_stories_front');
    }

    public function getDownloadBox($site)
    {
        if (!$t = $this->getRenderer())
            dtrigger_error('Error: Renderer is null');

        return $t->render('Central/download');
    }

}
