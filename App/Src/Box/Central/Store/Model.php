<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Model
 *
 * @author roza
 */

require_once 'Class/Model/Abstract.php';

class Class_Site_Box_Central_Store_Model extends Class_Model_Abstract {


	/**
	 * Return all delivery types from DB.
	 * @return array
	 */
	public function getDeliveryTypes()
	{
	    $db	= $this->getDb();

	    $sql = sprintf("SELECT * FROM %s
			    WHERE active = 1
			    ORDER BY description ASC;"
			,S_TABLE_STORE_DELIVERY_TYPES
		    );

	    $res = $db->query($sql);
	    $rws = $res->fetchAll();

	    return ($rws) ? $rws : NULL;
	}

	
	/**
	 * Return all prosucts from basket
	 * @return array
	 */
        public function getProductsFromBasket()
        {
	    $db	= $this->getDb();

            $sql = sprintf("SELECT b.session_id, b.count, p.* FROM %s b
                            LEFT JOIN %s p ON (b.product_id=p.product_id)
                            WHERE session_id = '%s'
                            AND b.user_ip = '%s'
                            ORDER BY created_time ASC;"
                        ,S_TABLE_STORE_BASKET
                        ,S_TABLE_STORE_PRODUCTS
                        ,session_id()
                        ,Class_Enviroment::getUserIp()
                    );

	    $res = $db->query($sql);
	    $rws = $res->fetchAll();

	    return ($rws) ? $rws : NULL;
        }
}

