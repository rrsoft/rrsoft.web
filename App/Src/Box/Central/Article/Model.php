<?php

/**
 * $Id: Model.php 85 2010-07-07 17:42:43Z bajt $
 * ----------------------------------------------
 * Data model of Central Story Class.
 *
 * @package RRsoft-CMS
 * @version $Rev: 30 $
 * @copyright (c) 2009-2010 RRsoft www.rrsoft.cz
 * @license GNU Public License
 *
 * Minimum Requirement: PHP 5.1.x
 */

namespace App\Box\Central\Article;

use Wbengine\Model\ModelAbstract;

class Model extends ModelAbstract {


    /**
     * Parent site object
     * @var Class_site
     */
    private $site = NULL;



    /**
     * Box abstrac class injection
     * @param \Wbengine\Box\BoxAbstract $boxAbstract
     */
    public function __construct($boxAbstract)
    {

	$this->site = $boxAbstract->getSite();
    }



    /**
     * Return story data from db
     * @return array
     */
    public function getArticleRow()
    {
	$where = array($this->site->getSiteId());

	$sql = sprintf("SELECT * FROM %s
			WHERE site_id = ?;"
		, S_TABLE_ARTICLES
	);

	return $this->getDbAdapter()->query($sql, $where);
    }



}
