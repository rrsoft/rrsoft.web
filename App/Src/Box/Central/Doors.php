<?php
/**
 * $Id: Doors.php 85 2010-07-07 17:42:43Z bajt $
 * ----------------------------------------------
 * Class Central Doors module class.
 *
 * @package RRsoft-CMS
 * @version $Rev: 30 $
 * @copyright (c) 2009-2010 RRsoft www.rrsoft.cz
 * @license GNU Public License
 *
 * Minimum Requirement: PHP 5.1.x
 */
require_once 'Class/Site/Box/Abstract.php';

class Class_Site_Box_Central_Doors extends Class_Site_Box_Abstract
{


    private $_navigace = array();



    /**
     * KATALOG - MAIN module
     * ------------------------
     * Return home box template
     *
     * @return string
     */
    public function getKatalogBox()
    {
        $params = $this->getBoxParamsFromUrl();

        /**
         * On multiply box url we can redirect user
         * allways to default box url.
         *
         * $this->setBoxHomeUrl('/katalog/');
         */

        // As default we return types box
        if ($this->getSite()->getUrl() === $this->getSite()->getLink())
        {
            return $this->getCategories();
        }


        switch (sizeof($params)) {
            case 1:
                return $this->getCategories();
                break;
            case 2:
                return $this->getTypes($params[1]);
                break;
            case 3:
                return $this->getDoors($params[2]);
                break;
            case 4:
                return $this->getDoorDetail($params[3]);
                break;
            default:
                break;


        }

        return $this->getRenderer()->render('Central/doors_categories', $content);
    }


    /**
     * KATALOG
     * -------------------------
     * Return all types template
     * @param string $kategorie
     * @return string
     */
    public function getTypes($kategorie = NULL)
    {
	$content =  $this->getModel()->getTypes($kategorie);
        $this->getRenderer()->setVar('home_url', $this->getBoxHomeUrl());

        $this->_navigace[] = array(
                        'url' => strtolower($this->getSite()->getUrl()),
                        'name' => strtolower($this->getModel()->getCategoryByKey($kategorie)),
                    );

        $this->getRenderer()->setVar('navigace', array_merge($this->getSite()->getNavigation(), $this->_navigace));

        // Because the box is not static, we need to set html title, keywords etc.
        $this->getSite()->setHtmlTitle('Vchodové dveře - '.$this->getModel()->getCategoryByKey($kategorie).' - seznam typů');
        $this->getSite()->setHtmlDescription('Typy vchodových dveří, seznam všech typů');

        return $this->getRenderer()->render('Central/doors_types', $content);
    }


    /**
     * KATALOG
     * ------------------------------
     * Return all Categotied template
     * @return string
     */
    public function getCategories()
    {
	$content =  $this->getModel()->getCategories();

        $this->getRenderer()->setVar('home_url', $this->getBoxHomeUrl());

        $this->getRenderer()->setVar('navigace', array_merge($this->getSite()->getNavigation(), $this->_navigace));

        return $this->getRenderer()->render('Central/doors_categories', $content);
    }


    /**
     * KATALOG
     * ----------------------------------
     * Return all products from given typ
     * and category.
     * @param string $typ
     * @return string
     */
    public function getDoors($typ)
    {
	$content =  $this->getModel()->getDoors($typ);

        $this->getRenderer()->setVar('home_url', $this->getBoxHomeUrl());

        $cat = $this->getModel()->getCategoryNameFromKeyType($typ);

        $this->_navigace[] = array(
                        'url' => strtolower($this->getBoxHomeUrl().$cat->key) . '/',
                        'name' => strtolower($cat->kategorie),
                    );
        $this->_navigace[] = array(
                        'url' => strtolower($this->getSite()->getUrl()),
                        'name' => strtolower($this->getModel()->getTypByKey($typ)),
                    );

        // Assign vars to template
        $this->getRenderer()->setVar('navigace', array_merge($this->getSite()->getNavigation(), $this->_navigace));
        $this->getRenderer()->setVar('kategorie', $cat->kategorie);
        $this->getRenderer()->setVar('typ', $this->getModel()->getTypByKey($typ));

        // Because the box is not static, we need to set html title, keywords etc.
        $this->getSite()->setHtmlTitle('Vchodové dveře - '.$cat->kategorie . ' - ' . $typ);
        $this->getSite()->setHtmlDescription('Katalog vchodových dveří - '.$cat->kategorie . ' - ' . $typ);
        $this->getSite()->setHtmlKeywords('Katalog vchodových dveří');

        return $this->getRenderer()->render('Central/doors_products', $content);
    }


    /**
     * KATALOG
     * -------------------------------
     * Return product detail template.
     * @param string $tag
     * @return string
     */
    public function getDoorDetail($tag)
    {return;
	$content =  $this->getModel()->getDoors($tag);

        $this->getRenderer()->setVar('home_url', $this->getBoxHomeUrl());



        return $this->getRenderer()->render('Central/doors_products', $content);
    }


    /**
     * Return box params from posted URL
     * @return array
     */
    private function getBoxParamsFromUrl()
    {
        $x = $this->getSite()->getLink();

        $y = explode('/', trim($x,'/'));

        return $y;
    }

}