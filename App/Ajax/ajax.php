<?php
    /**
     * $Id: Common.php 86 2010-07-08 13:26:12Z bajt $
     * ----------------------------------------------
     * Common including file. Here we including all
     * nesccessary files and classes.
     *
     * @package RRsoft-CMS
     * @version $Rev: 30 $
     * @copyright (c) 2009-2010 RRsoft www.rrsoft.cz
     * @license GNU Public License
     *
     * Minimum Requirement: PHP 5.1.x
     */

    if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || $_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest') {
        die(false);
    }

    // Disable all errors, except notices
    error_reporting ( E_ALL );

    $phpEx = substr(strrchr( __FILE__, '.'), 1 );

    // Session initialization
    session_start();

    require_once ('Zend/Mail.' . $phpEx);
    require_once ('Zend/Mail/Transport/Smtp.' . $phpEx);

    // This case we call mailer...
    include 'Ajax/Mail.php';